<?php

// главная
$app->get('', function () use ($app) {
    $events = $app ['event_service']->all ([], 'own_exhibition', true);
    $sponsors = $app ['sponsor_service']->all ([], true);
    $half = ceil(count ($events)/2);
    $content = file_get_contents(__DIR__ . '/data/mainfeed.html');

    return $app ['twig']->render ('index.twig', [
        'left_events'  => array_slice ($events, 0, 1),
        'right_events' => array_slice ($events, 1),
        'sponsors'     => $sponsors,
        'content'      => $content,
    ]);
});

// фестивали и выставки
$app->get('fests', function () use ($app) {
    $exhibitions = $app ['event_service']->all ([], 'exhibition', true);
    $festivals = $app ['event_service']->all ([], 'festival', true);
    $title = 'Фестивали & Выставки';

    return $app ['twig']->render ('fests.twig', array (
        'exhibitions'  => $exhibitions,
        'festivals' => $festivals,
        'title' => $title,
    ));
});

// запрос страницы события
$app->get('/event/{slug}', function ($slug) use ($app) {
    $event = $app ['event_service']->getBySlug ($slug);
    if (!$event || !$event ['visible']) $app->abort (404);

    $artists = $app ['event_service']->getArtists ($event['id'], true);

    return $app ['twig']->render ('event.twig', array (
        'event'  => $event,
        'artists'  => $artists,
        'slug'     => $slug,
    ));
})
->assert ('slug', '[a-z0-9_-]+');

// запрос страницы художника
$app->get('/artist/{slug}', function ($slug) use ($app) {
    $artist = $app ['artist_service']->getBySlug ($slug);
    if (!$artist || !$artist ['visible']) $app->abort (404);

    $limit    = $app ['gallery_limit'];
    $pictures = $app ['gallery_service']->all ($artist ['id'], 0, $limit);
    $total    = ceil ($app ['gallery_service']->total ($artist ['id']) / $limit);

    return $app ['twig']->render ('artist.twig', array (
        'artist'  => $artist,
        'pictures' => $pictures,
        'total'    => $total,
        'slug'     => $slug,
    ));
})
->assert ('slug', '[a-z0-9_-]+');

// запрос картинок для страницы художника
$app->get('/artist/{slug}/{offset}', function ($slug, $offset) use ($app) {
    $artist = $app ['artist_service']->getBySlug ($slug);
    if (!$artist || !$artist ['visible']) $app->abort (404);

    $limit    = $app ['gallery_limit'];
    $pictures = $app ['gallery_service']->all ($artist ['id'], $offset, $limit);

    return $app ['twig']->render ('pictures.twig', array (
        'pictures' => $pictures
    ));
})
->assert ('slug', '[a-z0-9_-]+')
->assert ('offset', '\d+');


// спонсоры
$app->get('/sponsors', function () use ($app) {
    $sponsors = $app ['sponsor_service']->all (array(), true);

    return $app ['twig']->render ('sponsors.twig', array (
        'sponsors'      => $sponsors,
    ));
});

// картинки
$app->get('/image/{type}/{key}', function ($type, $key) use ($app) {
    $app ['image_service']->get ($key, $type);
})
->assert ('type', '[\w_]+')
->assert ('key', '[0-9abcdef]+\.(jpg|png|gif)');

$app->get('/image/{type}/{size}/{key}', function ($type, $size, $key) use ($app) {
    $app ['image_service']->get ($key, $type, $size);
})
->assert ('type', '[\w_]+')
->assert ('key', '[0-9abcdef]+\.(jpg|png|gif)');

// контроллеры для админки
$app->mount('/admin', new app\controllers\adminControllerProvider);

// статичные страницы
$app->get('/{uri}',  function ($uri) use ($app) {
    $page = $app ['page_service']->get ($uri);

    if (!$page || $page ['hidden'] == 1) $app->abort (404);

    return $app ['twig']->render ('userpage.twig', array (
        'title' => $page ['title'],
        'content' => $page ['content']
    ));
})
->assert ('uri', "^(?!(admin))[^']+$");

$app->error(function (\Exception $e, $code) use ($app) {
    switch ($code) {
        case 404:
            $title = 'Страница не найдена';
            $message = 'Ой-ёй. По каким-то причинам этой страницы не существует';
            break;

        default:
            $title = 'Ошибка ' . $code;
            $message = 'Возникла ошибка с кодом ' . $code;
            if ($app ['debug']) {
                $message .= "<div style='background: white; padding: 5px; border: solid #444 1px; font: 13px Courier New; margin: 10px 0;'>{$e->getMessage()}</div>";
            }
    }
    if ($e->getMessage()) $message = $e->getMessage();

    header('', true, $code);
    return $app ['twig']->render ('userpage.twig', array (
        'title' => $title,
        'content' => $message
    ));
});