<?php
// конфигурация сайта
$app ['config'] = array (
    'domain'    => $_SERVER['HTTP_HOST'],
    'debug'     => true,
    'version'   => '1.2.11',
);

// различные параметры
$app ['debug'] = $app ['config']['debug'];
$app ['gallery_limit'] = 6;

// регистрируем пространство имён для своих классов
$app ['autoloader']->registerNamespace ('app', __DIR__ . '/../');

// регистрируем сервис Doctrine для доступа к БД
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'mysql_ruhoster' => $_SERVER ['SERVER_ADDR'] === '127.0.0.1' ?
            array (
                'driver'    => 'pdo_mysql',
                'host'      => 'localhost',//'93.81.243.79',
                'dbname'    => 'doskikraski',
                'user'      => 'doskikraski',
                'password'  => 'doski',
            ) : array (
                'driver'    => 'pdo_mysql',
                'host'      => 'localhost',
                'dbname'    => 'kirillsu_doski',
                'user'      => 'kirillsu_doski',
                'password'  => 'cZW33#j8',
            ),
    ),
    'db.dbal.class_path'    => MODROOT,
    'db.common.class_path'  => MODROOT,
));
$app ['db']->executeQuery ("SET NAMES 'utf8'");

// регистрируем сервис шаблонизатора twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path'       => APPROOT . '/view',
    'twig.class_path' => DOCROOT . '/modules/twig/lib',
    'twig.options'    => array ('strict_variables' => false),

));

// регистрируем сервисы
$app->register(new Silex\Provider\SessionServiceProvider);
$app->register(new app\services\userServiceProvider);
$app->register(new app\services\imageServiceProvider);
$app->register(new app\services\pageServiceProvider);
$app->register(new app\services\artistServiceProvider);
$app->register(new app\services\galleryServiceProvider);
$app->register(new app\services\sponsorServiceProvider);
$app->register(new app\services\eventServiceProvider);

// регистрируем обработчиков запросов
require_once 'routing.php';

$app->before(function () use ($app) {
    $uri = $app ['request']->getRequestUri ();
    $is_admin = strpos ($uri, '/admin/') === 0;

    // формируем список список пунктов главного меню
    $menu = array ();
    $pages = $app ['page_service']->all (null, array (), !$is_admin);
    $current = '';

    if ($is_admin) {
        $menu ['mainfeed'] = array ('title' => 'Главная', 'class' => 'mainfeed');
        $menu ['events'] = array ('title' => 'События', 'class' => 'events');
        $menu ['artists'] = array ('title' => 'Художники', 'class' => 'artists');
        $menu ['sponsors'] = array ('title' => 'Спонсоры', 'class' => 'sponsors');

        $uri = substr ($uri, 6);
    }

    foreach ($pages as $page) {
        $menu [$page ['uri']] = array (
            'title' => $page ['title'],
            'class' => 'userpage',
            'html_params' => 'data-id=' . $page ['id']
        );
    }

    if ($is_admin) {
        $menu ['userpage/new'] = array ('title' => '+ Добавить страницу', 'class' => 'green');
    } else {
        $menu ['sponsors'] = array ('title' => 'При поддержке', 'class' => 'sponsors');
    }

    foreach ($menu as $item_uri => $title) {
        if (strpos ($uri, $item_uri) === 1) $current = $item_uri;
    }

    $app ['twig']->addGlobal ('main_menu', $menu);
    $app ['twig']->addGlobal ('main_menu_current', $current);

    // для страниц админки проверяем авторизацию пользователя
    if ($is_admin) {
        $user = $app ['user_service']->check ();
        $app ['twig']->addGlobal ('user', $user);
    }

    // добавляем различные параметры сайта, необходимые для view
    // TODO: реализовать обновление ресурсных файлов (css и js) через манифесты
    $app ['twig']->addGlobal ('site_version', $app ['config'] ['version']);
});

// запускаем!
$app->run();