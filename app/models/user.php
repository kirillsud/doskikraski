<?php
namespace app\models;

/**
 * Модель данных о пользователе
 *
 * Описание виртуальных методов, соответствующим полям с именем на __:
 *
 * @method user|\string username(\string $username)
 */
class user extends base {
    protected $__username;

    public function __construct ($username = '') {
        $this->username ($username);
    }
}