<?php
namespace app\models;

/**
 * Базовый класс модели. Обрабатывает сеттеры и геттеры
 * для всех членов данных класса, начинающихся с двойного подчёркивания (__)
 *
 * Такое решение позволяет шаблонизатору получать значения
 * этих полей при обращении к ним во время рендеринга
 */
class base {

    function __call ($name, $args) {
        $name = '__' . $name;
        if (property_exists($this,$name)) {
            if (empty ($args))
                return $this->$name;
            else {
                $this->$name = $args [0];
                return $this;
            }
        }
    }
}
