<?php
namespace app\controllers;

use app\services\EventService;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

/**
 * Обрабатывает действия для админки
 */
class adminControllerProvider implements ControllerProviderInterface
{
    public function connect (Application $app)
    {
        $controllers = new ControllerCollection();

        // главная страница админки
        $controllers->get('/', function () use ($app) {
            return $app->redirect ('/admin/events');
        });

        // редактирование главной страницы
        $controllers->match('/mainfeed', function () use ($app) {
            $content = file_get_contents(realpath(__DIR__ . '/..') . '/data/mainfeed.html');

            if ($app ['request']->getMethod() === 'POST') {
                $content = $_POST ['mainfeed']['content'];
                file_put_contents(realpath(__DIR__ . '/..') . '/data/mainfeed.html', $content);
            }

            return $app ['twig']->render ('admin/mainfeed.twig', array (
                'content' => $content,
            ));
        });

        // предпросмотр главной страницы
        $controllers->post('/mainfeed/preview', function () use ($app) {
            $events = $app ['event_service']->all ([], 'own_exhibition', true);
            $sponsors = $app ['sponsor_service']->all ([], true);
            $half = ceil(count ($events)/2);
            $content = $_POST ['mainfeed']['content'];

            return $app ['twig']->render ('index.twig', [
                'left_events'  => array_slice ($events, 0, $half),
                'right_events' => array_slice ($events, $half),
                'sponsors'     => $sponsors,
                'content'      => $content,
            ]);
        });

        // список художников
        $controllers->get('/artists', function () use ($app) {
            $artists = $app ['artist_service']->all ();
            return $app ['twig']->render ('admin/artist/index.twig', array (
                'artists' => $artists
            ));
        });

        // добавление художника
        $controllers->match('/artists/add', function () use ($app) {
            $data = array ('name'=>'', 'image_key'=>'', 'visible'=>'', 'about'=>'');
            $errors = array ();

            if ($app ['request']->getMethod() === 'POST') {
                $post = $_POST ['artist'];

                if (isset ($_FILES ['image']) && $_FILES ['image']['tmp_name'])
                    $post  ['image'] = $_FILES ['image'];

                if ($app ['artist_service']->validate ($post)) {
                    $id = $app ['artist_service']->save ($post);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Художник добавлен'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ("/admin/artists");

                    return $app->redirect ('/admin/artists/' . $id . '/edit');
                }

                $data = array_merge($data, $post);
                $errors = $app ['artist_service']->errors ();
            }

            return $app ['twig']->render ('admin/artist/add.twig', array (
                'artist' => $data,
                'errors' => $errors
            ));
        });

        // редактирование художника
        $controllers->match('/artists/{id}/edit', function ($id) use ($app) {
            $data = $app ['artist_service']->get ($id);
            $errors = array ();

            if (!$data) $app->abort (404);

            if ($app ['request']->getMethod() === 'POST') {
                $post = $_POST ['artist'];

                if (isset ($_FILES ['image']) && $_FILES ['image']['tmp_name'])
                    $post  ['image'] = $_FILES ['image'];

                if ($app ['artist_service']->validate ($post)) {
                    $app ['artist_service']->save ($post);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Информация о художнике изменена'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ('/admin/artists');

                }

                $data = array_merge($data, $post);
                $errors = $app ['artist_service']->errors ();
            }

            return $app ['twig']->render ('admin/artist/edit.twig', array (
                'artist' => $data,
                'errors' => $errors,
            ));
        });

        // изменение флага видимости художника
        $controllers->match('/artists/{id}/visible', function ($id) use ($app) {
            $data = $app ['artist_service']->get ($id);
            if (!$data) $app->abort (404);

            $data ['visible'] = 1 - $data ['visible'];
            $app ['artist_service']->save ($data);
            return $app->redirect ('/admin/artists');

        });

        // удаление художника
        $controllers->get('/artists/{id}/delete', function ($id) use ($app) {
            $res = $app ['artist_service']->delete ((int) $id);
            if ($res)
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Художник удалён'));
            else
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Не удалось удалить художника'));

            return $app->redirect ('/admin/artists');
        });

        // удаление аватары художника
        $controllers->get('/artists/{id}/image/delete', function ($id) use ($app) {
            $data = $app ['artist_service']->get ($id);
            if (!$data) $app->abort (404);

            $res = $app ['artist_service']->deleteImage ($id);

            if ($res)
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Фотка художника удалёна'));
            else
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Не удалось удалить фотку художника'));

            return $app->redirect ("/admin/artists/{$id}/edit");
        });

        // изменение порядка артиста вверх
        $controllers->get('/artists/up/{id}', function ($id) use ($app) {
            $res = $app ['artist_service']->up ((int) $id);
            header ('Content-Type: application/json');
            echo json_encode(array ('success' => $res));
            die;
        });

        // изменение порядка артиста вниз
        $controllers->get('/artists/down/{id}', function ($id) use ($app) {
            $res = $app ['artist_service']->down ((int) $id);
            header ('Content-Type: application/json');
            echo json_encode(array ('success' => $res));
            die;
        });

        // список работ художника
        $controllers->get('/artists/{pid}/gallery', function($pid) use ($app) {
            $data = $app ['gallery_service']->all ($pid);

            return $app ['twig']->render ('/admin/gallery/index.twig', array (
                'gallery' => $data, 'artist' => $pid
            ));
        });

        // добавление работы
        $controllers->match('/artists/{pid}/gallery/add', function($pid) use ($app) {
            $picture = array ('image_key' => '', 'title' => '', 'description' => '', 'artist' => $pid);
            $errors = array();

            if ($app ['request']->getMethod() === 'POST') {
                $picture = $_POST ['picture'];
                $picture ['image'] = $_FILES ['image'];

                if ($app ['gallery_service']->validate ($picture)) {
                    $id = $app ['gallery_service']->save ($picture);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Работа добавлена'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ("/admin/artists/$pid/gallery");
                }

                if (!in_array ('image_key', $picture)) $picture ['image_key'] = '';
                $errors = $app ['gallery_service']->errors ();
            }

            return $app ['twig']->render ('/admin/gallery/add.twig', array (
                'picture' => $picture, 'artist' => $pid, 'errors' => $errors
            ));
        });

        // редактирование работы
        $controllers->match('/artists/{pid}/gallery/edit/{id}', function($pid, $id) use ($app) {
            $picture = $app ['gallery_service']->get ((int)$id);
            $errors = array();

            if ($app ['request']->getMethod() === 'POST') {
                $data = $_POST ['picture'];
                $after_submit = $_POST ['after_submit'];
                if (isset ($_FILES ['image']) && $_FILES ['image']['tmp_name'])
                    $data ['image'] = $_FILES ['image'];

                if ($app ['gallery_service']->validate ($data)) {
                    $id = $app ['gallery_service']->save ($data, $picture);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Работа сохранена'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ("/admin/artists/$pid/gallery");
                }

                $errors = $app ['gallery_service']->errors ();
            }

            return $app ['twig']->render ('/admin/gallery/edit.twig', array (
                'picture' => $picture, 'artist' => $pid, 'errors' => $errors
            ));
        });

        // удаление работы
        $controllers->get('/artists/{pid}/gallery/delete/{id}', function($pid, $id) use ($app) {
            $res = $app ['gallery_service']->delete ((int) $id);
            if ($res)
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Работа удалёна'));
            else
                $app ['session']->set ('flash', array ('type' => 'error', 'message' => 'Не удалось удалить работу'));

            return $app->redirect ('/admin/artists/' . $pid . '/gallery');
        });

        // изменение порядка работы
        $controllers->get('/artists/{pid}/gallery/up/{id}', function ($pid, $id) use ($app) {
            $res = $app ['gallery_service']->up ((int) $id);
            header ('Content-Type: application/json');
            echo json_encode(array ('success' => $res));
            die;
        });

        // изменение порядка работы
        $controllers->get('/artists/{pid}/gallery/down/{id}', function ($pid, $id) use ($app) {
            $res = $app ['gallery_service']->down ((int) $id);
            header ('Content-Type: application/json');
            echo json_encode(array ('success' => $res));
            die;
        });

        // список спонсоров
        $controllers->get('/sponsors', function () use ($app) {
            $data = $app ['sponsor_service']->all ();
            return $app ['twig']->render ('admin/sponsor/index.twig', array (
                'data' => $data
            ));
        });

        // добавление спонсора
        $controllers->match('/sponsors/add', function () use ($app) {
            $data = array ('name'=>'', 'image_key'=>'', 'visible'=>'', 'about'=>'');
            $errors = array ();

            if ($app ['request']->getMethod() === 'POST') {
                $post = $_POST ['data'];

                if (isset ($_FILES ['image']) && $_FILES ['image']['tmp_name'])
                    $post  ['image'] = $_FILES ['image'];

                if ($app ['sponsor_service']->validate ($post)) {
                    $id = $app ['sponsor_service']->save ($post);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Художник добавлен'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ("/admin/sponsors");

                    return $app->redirect ('/admin/sponsors/' . $id . '/edit');
                }

                $data = array_merge($data, $post);
                $errors = $app ['sponsor_service']->errors ();
            }

            return $app ['twig']->render ('admin/sponsor/add.twig', array (
                'data' => $data,
                'errors' => $errors
            ));
        });

        // редактирование спонсора
        $controllers->match('/sponsors/{id}/edit', function ($id) use ($app) {
            $data = $app ['sponsor_service']->get ($id);
            $errors = array ();

            if (!$data) $app->abort (404);

            if ($app ['request']->getMethod() === 'POST') {
                $post = $_POST ['data'];

                if (isset ($_FILES ['image']) && $_FILES ['image']['tmp_name'])
                    $post  ['image'] = $_FILES ['image'];

                if ($app ['sponsor_service']->validate ($post)) {
                    $app ['sponsor_service']->save ($post);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Информация о художнике изменена'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ('/admin/sponsors');

                }

                $data = array_merge($data, $post);
                $errors = $app ['sponsor_service']->errors ();
            }

            return $app ['twig']->render ('admin/sponsor/edit.twig', array (
                'data' => $data,
                'errors' => $errors,
            ));
        });

        // изменение флага видимости спонсора
        $controllers->match('/sponsors/{id}/visible', function ($id) use ($app) {
            $artist = $app ['sponsor_service']->get ($id);
            if (!$artist) $app->abort (404);

            $artist ['visible'] = 1 - $artist ['visible'];
            $app ['sponsor_service']->save ($artist);
            return $app->redirect ('/admin/sponsors');

        });

        // удаление спонсора
        $controllers->get('/sponsors/{id}/delete', function ($id) use ($app) {
            $res = $app ['sponsor_service']->delete ((int) $id);
            if ($res)
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Художник удалён'));
            else
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Не удалось удалить спонсора'));

            return $app->redirect ('/admin/sponsors');
        });

        // список событий
        $controllers->get('/events', function () use ($app) {
            $data = $app ['event_service']->all ();
            return $app ['twig']->render ('admin/event/index.twig', array (
                'data' => $data,
                'types' => EventService::$types,
            ));
        });

        // добавление события
        $controllers->match('/events/add', function () use ($app) {
            $data = array ('name'=>'', 'slug'=>'', 'type'=>'', 'date'=>'', 'visible'=>'', 'artists'=>[], 'content'=>'');
            $errors = array ();

            if ($app ['request']->getMethod() === 'POST') {
                $post = $_POST ['event'];

                if ($app ['event_service']->validate ($post)) {
                    $id = $app ['event_service']->save ($post);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Событие добавлено'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ("/admin/events");

                    return $app->redirect ('/admin/events/' . $id . '/edit');
                }

                $data = array_merge($data, $post);
                $errors = $app ['event_service']->errors ();
            }

            $artists = [];
            foreach ($app ['artist_service']->all (['order' => 'name']) as $item)
                $artists [$item['name']] = $item['id'];

            return $app ['twig']->render ('admin/event/add.twig', array (
                'data' => $data,
                'errors' => $errors,
                'domain' => $app ['config']['domain'],
                'artists' => $artists,
                'types' => array_flip(EventService::$types),
            ));
        });

        // редактирование спонсора
        $controllers->match('/events/{id}/edit', function ($id) use ($app) {
            $data = $app ['event_service']->get ($id);
            $data['artists'] = $app ['event_service']->getArtists ($id);

            $errors = array ();

            if (!$data) $app->abort (404);

            if ($app ['request']->getMethod() === 'POST') {
                $post = $_POST ['event'];

                if ($app ['event_service']->validate ($post)) {
                    $app ['event_service']->save ($post);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Информация о событии изменена'));

                    if (isset ($_POST ['save_and_list']))
                        return $app->redirect ('/admin/events');

                }

                $data = array_merge($data, $post);
                $errors = $app ['event_service']->errors ();
            }

            $artists = [];
            foreach ($app ['artist_service']->all (['order' => 'name']) as $item)
                $artists [$item['name']] = $item['id'];

            return $app ['twig']->render ('admin/event/edit.twig', array (
                'data' => $data,
                'errors' => $errors,
                'domain' => $app ['config']['domain'],
                'artists' => $artists,
                'types' => array_flip(EventService::$types),
            ));
        });

        // изменение флага видимости события
        $controllers->match('/events/{id}/visible', function ($id) use ($app) {
            $data = $app ['event_service']->get ($id);
            if (!$data) $app->abort (404);

            $data ['visible'] = 1 - $data ['visible'];
            $app ['event_service']->save ($data);
            return $app->redirect ('/admin/events');

        });

        // удаление события
        $controllers->get('/events/{id}/delete', function ($id) use ($app) {
            $res = $app ['event_service']->delete ((int) $id);
            if ($res)
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Событие удалёно'));
            else
                $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Не удалось удалить событие'));

            return $app->redirect ('/admin/events');
        });

        // предпросмотр события
        $controllers->post('/events/preview', function () use ($app) {
            $data = $_POST ['event'];
            $app ['event_service']->validate ($data);
            $errors = $app ['event_service']->errors ();

            $artists = [];
            if (isset($data['artists'])) {
                foreach ($app ['artist_service']->all (['order' => 'name']) as $item) {
                    $id = $item['id'];
                    if (!in_array($id, $data['artists'])) continue;

                    $artists [] = $item;
                }
            }

            // выключаем защиту от XSS
            header('X-XSS-Protection: 0');

            return $app ['twig']->render ('event.twig', array (
                'event'  => $data,
                'artists'  => $artists,
                'errors' => $errors,
            ));
        });

        // изменение порядка артиста вверх
        $controllers->get('/sponsors/up/{id}', function ($id) use ($app) {
            $res = $app ['sponsor_service']->up ((int) $id);
            header ('Content-Type: application/json');
            echo json_encode(array ('success' => $res));
            die;
        });

        // изменение порядка артиста вниз
        $controllers->get('/sponsors/down/{id}', function ($id) use ($app) {
            $res = $app ['sponsor_service']->down ((int) $id);
            header ('Content-Type: application/json');
            echo json_encode(array ('success' => $res));
            die;
        });

        // страница выхода для админа
        $controllers->get('/logout', function () use ($app) {
            $app ['user_service']->logout ();
            return $app->redirect ('/');
        });

        // добавление пользовательской страницы
        $controllers->match('/userpage/new', function () use ($app) {
            $page = array ();

            if ($app ['request']->getMethod() === 'POST') {
                $data = $_POST ['page'];

                if ($app ['page_service']->validate ($data)) {
                    $app ['page_service']->save ($data);
                    $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Изменения страницы сохранёны'));
                    return $app->redirect ('/admin/' . $data ['uri']);
                } else {
                    $app ['twig']->addGlobal ('errors', $app ['page_service']->errors ());
                    $page = $data;
                }
            }

            return $app ['twig']->render ('/admin/userpage/add.twig', array (
                'page' => $page,
                'domain' => $app ['config']['domain']
            ));
        });

        // изменение порядка пунктов пользовательского меню
        $controllers->post ('/userpage/order', function () use ($app) {
            $data = $_POST ['order'];
            $order = array ();
            $min = 0;
            foreach ($data as $index => $id) {
                if ($id == "undefined") {
                    $min = $index;
                    continue;
                }

                $order [$id] = $index - $min;
            }
            $app ['page_service']->order ($order);
        });

        // предпросмотр пользовательской страницы
        $controllers->post ('/userpage/preview', function () use ($app) {
            $data = $_POST ['page'];

            // выключаем защиту от XSS
            header('X-XSS-Protection: 0');

            return $app ['twig']->render ('userpage.twig', array (
                'title' => $data['title'],
                'content' => $data['content'],
            ));
        });

        // редактирование и удаление пользовательской страницы
        $controllers->match('/{uri}', function ($uri) use ($app) {
            $page = $app ['page_service']->get ($uri, false);
            if (!$page) $app->abort (404);

            if ($app ['request']->getMethod() === 'POST') {
                $data = $_POST ['page'];

                // удаление
                if (isset ($_POST ['delete'])) {
                    if ($app ['page_service']->delete ($data ['id'])) {
                        $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Страница удалена'));
                        return $app->redirect ('/admin');
                    } else {
                        $app ['session']->set ('flash', array ('type' => 'error', 'message' => 'Невозможно удалить страницу "' . $data ['title'] . '"'));
                    }
                }
                // редактирование
                else {
                    if ($app ['page_service']->validate ($data)) {
                        $app ['page_service']->save ($data);
                        $app ['session']->set ('flash', array ('type' => 'success', 'message' => 'Изменения страницы сохранёны'));
                        return $app->redirect ('/admin/' . $data ['uri']);
                    } else {
                        $app ['twig']->addGlobal ('errors', $app ['page_service']->errors ());
                    }
                }

                $page = array_merge ($page, $data);
            }

            return $app ['twig']->render ('/admin/userpage/edit.twig', array (
                'page' => $page,
                'domain' => $app ['config']['domain']
            ));

        })->assert ('uri', "[^']+");

        return $controllers;
    }
}
