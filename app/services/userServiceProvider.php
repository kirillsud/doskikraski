<?php
namespace app\services;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Сервис пользователя. Необходим для отслеживания авторизированного пользователя.
 * Т.е. проверяет, авторизирован ли кто-то в системе,
 * выполняет авторизицию и выход пользователя
 */
class userServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['user_service'] = $app->share(function () use ($app) {
            return new UserService ($app);
        });
    }
}

class UserService {
    /** @var \Silex\Application */
    private $app;
    /** @var \app\models\user */
    private $user;

    public function __construct (Application $app) {
        $this->app = $app;
        $this->user = $app ['session']->get('user');
    }

    /**
     * Проверяет авторизацию пользователя в системе,
     * если её нет, просит ввести логин и пароль для входа
     *
     * @return array данные пользователя
     */
    public function check () {
        if (null === $this->user) {
            $username = $this->app ['request']->server->get('PHP_AUTH_USER');
            $password = $this->app ['request']->server->get('PHP_AUTH_PW');

            if ('kraski' !== $username || 'dk1020' !== $password) {
                $response = new Response();
                $response->headers->set('WWW-Authenticate', sprintf('Basic realm="%s"', 'site_login'));
                $response->setStatusCode(401, 'Please sign in.');
                $response->send();
                exit;
            } else {
                $this->login($username);
            }
        }

        return $this->user;
    }

    /**
     * Авторизирует пользователя
     * @param string $username  имя пользователя
     */
    public function login ($username) {
        $this->user = new \app\models\user ($username);
        $this->app['session']->set('user', $this->user);

        if (!isset ($_SESSION['KCFINDER'])) $_SESSION['KCFINDER'] = array();
        $_SESSION['KCFINDER']['disabled'] = false;
    }

    /**
     * Выполняет выход пользователя из системы
     */
    public function logout () {
        $this->user = null;
        $this->app ['session']->set ('user', $this->user);

        unset ($_SESSION['KCFINDER']);
    }
}