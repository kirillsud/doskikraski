<?php
namespace app\services;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Сервис галереи рисунков художника. Позволяет получать рисунки из галереи художника,
 * изменять, добавлять и удалять.
 */
class galleryServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    {
        $app['gallery_service'] = $app->share(function () use ($app) {
            return new GalleryService($app);
        });
    }
}

class GalleryService {
    /** @var \Silex\Application */
    private $app;

    /** @var ImageService */
    private $imageService;

    private $lastErrors;
    private $filePath;
    private $imageSection;

    public function __construct (Application $app) {
        $this->app = $app;
        $this->imageService = $app ['image_service'];
        $this->imageSection = 'gallery';
        $this->filePath = $this->imageService->getUploadPath() . $this->imageSection;
    }

    public function all ($artist, $offset = 0, $limit = 5, $only_visible = false) {
        $artist = (int)$artist;
        $offset = (int)$offset * $limit;
        $limit = (int)$limit;
        $where = $only_visible ? "AND visible = 1 " : "";
        $sql = "SELECT * FROM images WHERE artist = $artist $where ORDER BY sort_order DESC LIMIT $offset, $limit";
        $data = $this->app['db']->fetchAll ($sql);

        return $data;
    }

    public function total ($artist, $only_visible = false) {
        $artist = (int)$artist;
        $where = $only_visible ? "AND visible = 1" : "";
        $sql = "SELECT COUNT(*) FROM images WHERE artist = $artist $where";
        $data = $this->app['db']->fetchArray ($sql);
        return (int)$data [0];
    }

    public function get ($id) {
        $sql = "SELECT * FROM images WHERE id = ?";
        $data = $this->app['db']->fetchAssoc ($sql, array($id));
        if (!$data) return false;

        return $data;
    }

    public function validate (&$data) {
        $this->lastErrors = array ();

        $data ['artist'] = (int) $data ['artist'];

        if (!isset ($data ['artist']) || $data ['artist'] < 1) {
            $this->lastErrors ['artist'] = 'Нужно указать художника';
            return false;
        }

        if (!isset ($data ['id']) && (!isset ($data ['image']) || !$data ['image']['tmp_name'])) {
            $this->lastErrors ['image'] = 'Без картинки ни как не сохранить';
            return false;
        }

        if (isset ($data ['image'])) {
            $extension = null;
            switch (exif_imagetype ($data ['image']['tmp_name'])) {
                case IMAGETYPE_GIF:
                    $extension = 'gif';
                    break;

                case IMAGETYPE_JPEG:
                    $extension = 'jpg';
                    break;

                case IMAGETYPE_PNG:
                    $extension = 'png';
                    break;

                default:
                    $this->lastErrors ['image'] = 'Возможно загрузить изображение только следующих форматов: JPG, PNG, GIF';
                    return false;
            }

            $data ['image']['extension'] = $extension;
        }

        if (isset ($data ['title'])) {
            $data ['title'] = trim (strip_tags ($data ['title']));

            if (mb_strlen ($data ['title'], 'utf8') > 200) {
                $this->lastErrors ['title'] = 'Длина заголовка не должна быть больше 200 символов';
                return false;
            }
        }

        if (isset ($data ['description']))
            $data ['description'] = strip_tags ($data ['description']);

        $data ['visible'] = isset ($data ['visible']) ? 1 : 0;

        return empty ($this->lastErrors);
    }

    public function save ($data, $object = null) {
        if (isset ($data ['image'])) {
            if ($object && $object ['image_key']) {
                $this->imageService->delete ($object ['image_key'], $this->imageSection);
            }

            // TODO: Заменить методом загрузки из сервиса картинок
            if (!is_dir ($this->filePath)) mkdir ($this->filePath, 0775, true);
            $imageKey = ($object && $object ['image_key']) ? $object ['image_key'] :
                (md5 (time ()) . '.' . pathinfo ($data ['image']['name'], PATHINFO_EXTENSION));
            move_uploaded_file ($data ['image']['tmp_name'], $this->filePath . '/' . $imageKey );

            $data ['image_key'] = $imageKey;
            unset ($data ['image']);
        }

        if (isset ($data ['id'])) {
            $this->app ['db']->update ('images', $data, array ('id' => $data ['id']));
            return $data ['id'];
        } else {
            // получаем максимальный номер порядка картинок для конкретной галереи
            $artist = $data ['artist'];
            $last_sort_order = $this->app ['db']->fetchColumn ("SELECT MAX(`sort_order`) FROM images WHERE `artist` = $artist");
            $data ['sort_order'] = $last_sort_order + 1;

            $this->app ['db']->insert ('images', $data);

            try {
                return $this->app [ 'db' ]->fetchColumn('SELECT LAST_INSERT_ID ()');
            } catch (Exception $e) {
                return 0;
            }
        }
    }

    public function delete ($id) {
        if (false === $data = $this->get ($id)) return false;
        $imageKey = $data ['image_key'];

        if ($this->app ['db']->delete ('images', array ('id' => (int) $id)) === 0) return false;
        $this->imageService->delete ($imageKey, $this->imageSection);
        return true;
    }

    public function up ($id) {
        if (false === $image1 = $this->get ($id)) return false;

        $artist = $image1 ['artist'];
        $sql = "SELECT * FROM images WHERE sort_order > ? AND artist = $artist ORDER BY sort_order ASC LIMIT 1";
        if (!$image2 = $this->app['db']->fetchAssoc ($sql, array($image1 ['sort_order']))) return true;

        list ($image1 ['sort_order'], $image2 ['sort_order']) = array ($image2 ['sort_order'], $image1 ['sort_order']);
        $this->save($image1);
        $this->save($image2);
        return true;
    }

    public function down ($id) {
        if (false === $image1 = $this->get ($id)) return false;

        $artist = $image1 ['artist'];
        $sql = "SELECT * FROM images WHERE sort_order < ? AND artist = $artist ORDER BY sort_order DESC LIMIT 1";
        if (!$image2 = $this->app['db']->fetchAssoc ($sql, array($image1 ['sort_order']))) return true;

        list ($image1 ['sort_order'], $image2 ['sort_order']) = array ($image2 ['sort_order'], $image1 ['sort_order']);
        $this->save($image1);
        $this->save($image2);
        return true;
    }

    public function errors () {
        return $this->lastErrors;
    }
}
