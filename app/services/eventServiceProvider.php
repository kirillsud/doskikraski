<?php
namespace app\services;

use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Сервис событий. Позволяет получать информацию событии,
 * художниках, принимавших участие в событии
 */
class eventServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['event_service'] = $app->share(function () use ($app) {
            return new EventService ($app);
        });
    }
}

class EventService {
    /** @var \Silex\Application */
    private $app;

    /** @var \Doctrine\DBAL\Connection */
    private $db;

    private $lastErrors;
    private $table;
    private $artists_table;

    public function __construct (Application $app) {
        $this->app = $app;
        $this->db = $app ['db'];
        $this->table = 'events';
        $this->artists_table = 'artists_events';
    }

    public function all (array $params = array(), $type = null, $only_visible = false) {
        $limit = $params ? "LIMIT {$params ['offset']}, {$params ['limit']}" : "";
        $where = $this->filter( $type, $only_visible );
        $sql = "SELECT * FROM {$this->table} $where ORDER BY date DESC, timestamp DESC " . $limit;
        $data = $this->db->fetchAll ($sql);
        return $data;
    }

    public function total ($type = null, $only_visible = false) {
        $where = $this->filter( $type, $only_visible );
        $sql = "SELECT count(*) FROM {$this->table} $where";
        $data = $this->db->fetchArray ($sql);
        return (int)$data [0];
    }

    public function get ($id) {
        $sql = "SELECT * FROM {$this->table} WHERE id = ?";
        $data = $this->db->fetchAssoc ($sql, array($id));
        return $data ? $data : false;
    }

    public function getBySlug ($slug) {
        $sql = "SELECT * FROM {$this->table} WHERE slug = ?";
        $data = $this->db->fetchAssoc ($sql, array($slug));
        return $data ? $data : false;
    }

    public function getArtists ($id, $full = false) {
        $join = $full ? "RIGHT JOIN artists a ON t.artist_id = a.id" : "";
        $columns = $full ? "a.*" : "artist_id";
        $sql = "SELECT {$columns} FROM {$this->artists_table} t {$join} WHERE event_id = ?";
        $data = $this->db->fetchAll ($sql, array((int)$id));
        return $data ? ($full ? $data : array_map( function($item) { return $item["artist_id"]; }, $data)) : [];
    }

    public function validate (&$data) {
        $this->lastErrors = array ();
        $data ['name'] = strip_tags ($data ['name']);
        $data ['slug'] = mb_strtolower (strip_tags ($data ['slug']), 'utf-8');
        $data ['type'] = mb_strtolower (strip_tags ($data ['type']), 'utf-8');

        if (mb_strlen ($data ['name'], 'utf8') > 200) {
            $this->lastErrors ['name'] = 'Название события не может быть больше 200 символов';
            return false;
        }

        if (mb_strlen ($data ['slug'], 'utf8') > 50) {
            $this->lastErrors ['slug'] = 'Адрес страницы события не может быть больше 50 символов';
            return false;
        }

        if (!preg_match('#^[a-z_0-9-]+$#', $data ['slug'])) {
            $this->lastErrors ['slug'] = 'Адрес страницы события должен состоять только из символов английского алфавита, цифр и символов _ и -';
            return false;
        }

        if (!in_array( $data ['type'], array_keys(self::$types) )) {
            $this->lastErrors ['type'] = 'Укажите тип события';
            return false;
        }

        if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $data ['date'])) {
            $this->lastErrors ['date'] = 'Дата события указана неверно';
            return false;
        }

        $data ['visible'] = isset ($data ['visible']) ? 1 : 0;

        return empty ($this->lastErrors);
    }

    public function save ($data) {
        $artists = $data ['artists'];
        unset ($data ['artists']);

        if (isset ($data ['id'])) {
            $this->db->update ($this->table, $data, array ('id' => $data ['id']));
        } else {
            // получаем максимальный номер порядка артистов
            //$last_sort_order = $this->db->fetchColumn ("SELECT MAX(`sort_order`) FROM {$this->table}");
            //$data ['sort_order'] = $last_sort_order + 1;

            $this->db->insert ($this->table, $data);
            $data['id'] = (int) $this->db->fetchArray ('SELECT LAST_INSERT_ID ()')[0];
        }

        $this->updateArtists( $data['id'], $artists );
        return (int) $data['id'];
    }

    public function delete ($id) {
        if (false === $data = $this->get ($id)) return false;

        if ($this->db->delete ($this->table, array ('id' => (int)$id)) === 0)
            return false;

        return true;
    }

    public function up ($id) {
        if (false === $data1 = $this->get ($id)) return false;

        $sql = "SELECT * FROM {$this->table} WHERE sort_order > ? ORDER BY sort_order ASC LIMIT 1";
        if (!$data2 = $this->db->fetchAssoc ($sql, array($data1 ['sort_order']))) return true;

        list ($data1 ['sort_order'], $data2 ['sort_order']) = array ($data2 ['sort_order'], $data1 ['sort_order']);
        $this->save($data1);
        $this->save($data2);
        return true;
    }

    public function down ($id) {
        if (false === $data1 = $this->get ($id)) return false;

        $sql = "SELECT * FROM {$this->table} WHERE sort_order < ? ORDER BY sort_order DESC LIMIT 1";
        if (!$data2 = $this->db->fetchAssoc ($sql, array($data1 ['sort_order']))) return true;

        list ($data1 ['sort_order'], $data2 ['sort_order']) = array ($data2 ['sort_order'], $data1 ['sort_order']);
        $this->save($data1);
        $this->save($data2);
        return true;
    }

    public function errors () {
        return $this->lastErrors;
    }

    /**
     * @param $type
     * @param $visible
     * @return array|string
     */
    private function filter( $type, $visible )
    {
        $where = [];

        if ($visible) {
            $where[] = "visible = 1";
        }

        if ($type) {
            if (!is_array($type)) {
                $type = [$type];
            }
            $where[] = "type IN ('" . implode("', '", $type) . "')";
        }

        $where = $where ? ("WHERE " . implode( " AND ", $where )) : "";
        return $where;
    }

    public static $types = [
        'own_exhibition' => 'Собственная выставка',
        'exhibition' => 'Чужая выставка',
        'festival' => 'Фестиваль',
    ];

    private function updateArtists($id, $artists)
    {
        $this->db->beginTransaction();
        $this->db->delete( $this->artists_table, array ('event_id' => $id) );
        foreach ($artists as $artist) {
            $this->db->insert( $this->artists_table, array ('event_id' => $id, 'artist_id' => (int)$artist) );
        }
        $this->db->commit();
    }
}
