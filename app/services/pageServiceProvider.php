<?php
namespace app\services;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;

class pageServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    {
        $app['page_service'] = $app->share(function () use ($app) {
            return new PageService ($app);
        });
    }
}

class PageService {
    /** @var \Silex\Application */
    private $app;
    private $last_errors;

    public function __construct (Application $app) {
        $this->app = $app;
    }

    public function all ($parent_id = null, array $params = array(), $only_visible = false) {
        $limit = $params ? "LIMIT {$params ['offset']}, {$params ['limit']}" : "";
        $where = "WHERE 1 ";
        if ($parent_id) $where .= " AND parent_id = ?";
        if ($only_visible) $where .= " AND hidden = 0";
        $sql = "SELECT id, uri, title FROM userpages $where " . $limit . " ORDER BY `order`";
        $data = $this->app['db']->fetchAll ($sql, array ($parent_id));

        return $data;
    }

    public function total ($only_visible = false) {
        $where = $only_visible ? " WHERE hidden = 0" : "";
        $sql = "SELECT count(*) FROM userpages $where";
        $data = $this->app['db']->fetchArray ($sql);
        return (int)$data [0];
    }

    public function get ($uri, $format = false) {
        $uri = str_replace ("'", "\\'", $uri);
        $sql = "SELECT * FROM userpages WHERE uri = ?";
        $data = $this->app['db']->fetchAssoc ($sql, array($uri));
        if (!$data) return false;

        return $data;
    }

    public function validate (&$data) {
        $this->last_errors = array ();

        $data ['title'] = trim (strip_tags ($data ['title']));
        $data ['uri'] = trim (strip_tags ($data ['uri']));

        if (!$data ['title']) $this->last_errors ['title'] = 'Обязательно нужен заголовок';
        if (!$data ['uri']) $this->last_errors ['uri'] = 'Обязательно нужен адрес';
        if (preg_match('/[\/\\\'"@]/', $data ['uri'])) $this->last_errors ['uri'] = 'Адрес страницы не может сожержать символы \\, /, \', ", @';

        if (isset ($data ['id']) && $data ['id']) {
            $same = $this->get ($data ['uri']);
            if ($same && $same ['id'] != $data ['id'])
                $this->last_errors ['uri'] = 'Страница с таким адресом уже существует';
        }

        $data ['hidden'] = isset ($data ['hidden']) ? 1 : 0;

        return empty ($this->last_errors);
    }

    public function save ($data) {
        if (isset ($data ['id'])) {
            $this->app ['db']->update ('userpages', $data, array ('id' => $data ['id']));
            return $data ['id'];
        } else {
            $this->app ['db']->insert ('userpages', $data);
            $data = $this->app ['db']->fetchArray ('SELECT LAST_INSERT_ID ()');
            return (int) $data [0];
        }
    }

    public function delete ($id) {
        return $this->app ['db']->delete ('userpages', array ('id' => (int) $id)) && true;
    }

    public function order ($order) {
        // TODO: обновлять только изменёные элементы
        foreach ($order as $id => $index) {
            $this->save (array ('id' => $id, '`order`' => $index));
        }
    }

    public function errors () {
        return $this->last_errors;
    }
}
