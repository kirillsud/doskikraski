<?php
namespace app\services;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Сервис пользователя. Необходим для отслеживания авторизированного пользователя.
 * Т.е. проверяет, авторизирован ли кто-то в системе,
 * выполняет авторизицию и выход пользователя
 */
class imageServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['image_service'] = $app->share(function () use ($app) {
            return new ImageService ($app);
        });
    }
}

class ImageService {
    /** @var \Silex\Application */
    private $app;
    private $upload_path = '/media/uploads/';

    public function __construct (Application $app) {
        $this->upload_path = DOCROOT . '/media/uploads/';
        $this->app = $app;
    }

    protected $mimeTypes = array(
        'image/jpeg' => array('jpeg', 'jpg'),
        'image/png' => array('png')
    );

    protected $sizes = array(
        'main_feed'     => array (650,450, 'crop_height'),
        'admin_list'    => array (40,40, 'crop_biggest'),
        'admin_edit'    => array (0,200, 'fit_width'),
        'avatar'        => array (200,200, 'crop_biggest'),
        'sponsor_main'  => array (null,60, 'fit_width'),
        'sponsor_big'   => array (null,200, 'fit_width'),
    );

    protected $sections = array(
        'artists',
        'gallery',
        'sponsors',
    );

    public function get ($filekey, $section, $size = null) {
        $path = $this->upload_path . $section . '/' . $filekey;
        if (!is_file($path)) $this->app->abort (404);

        if ($size) {
            $path = $this->_resizeImage($path, $size);
        }

        $this->_returnImage($path);
    }

    public function upload() {
        // TODO: Filter extensions and etc

        $type = $this->request->param('type');

        if (!in_array($type, $this->sections)) {
            $this->notfound();
        }

        $data = array();

        $files = new Service_Files;
        $files->handle_file($data, $type, false);

        if (sizeof($data)) {
            $result = Service_Files::extract_file($data ['file'], $type);
        } else {
            $result = null;
        }

        $this->returnJSON($result);
    }

    protected function _resizeImage($path, $size) {
        list ($width, $height, $method) = $this->_checkSize($size);
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $filename = pathinfo($path, PATHINFO_FILENAME);

        $thumb = dirname($path) . '/' . $filename . '_' . sha1($width . '_' . $height . '_' . $method) . '.' . $extension;
        if (is_file ($thumb)) return $thumb;

        $imageInfo = getimagesize($path);
        $imageX = 0;
        $imageY = 0;
        $imageWidth = $imageInfo[0];
        $imageHeight = $imageInfo[1];

        if (is_dir(dirname($path)) === false) {
            mkdir(dirname($path), 0777, true);
        }

        switch ($method) {
            case 'crop_biggest':
                $tmpWidth = round ($imageHeight * ($width / $height));
                $tmpHeight = round ($imageWidth * ($height / $width));

                $method = $tmpWidth / $tmpHeight > $width / $height ? 'crop_height' : 'crop_width';
                break;

            case 'fit_biggest':
                $method = $imageWidth > $imageHeight ? 'fit_width' : 'fit_height';
                break;
        }


        switch ($method) {
            case 'crop_width':
                $tmpWidth = round ($imageHeight * ($width / $height));
                $imageX = ($imageWidth - $tmpWidth) / 2;
                $imageWidth = $tmpWidth;
                break;

            case 'crop_height':
                $tmpHeight = round ($imageWidth * ($height / $width));
                $imageY = ($imageHeight - $tmpHeight) / 2;
                $imageHeight = $tmpHeight;
                break;

            case 'fit_width':
                $width = round ($height * ($imageWidth / $imageHeight));
                break;

            case 'fit_height':
                $height = round ($width * ($imageHeight / $imageWidth));
                break;
        }

        if ($extension == 'png') {
          $src = imagecreatefrompng($path);
          $tmp = imagecreatetruecolor ($width, $height);
          imagecopyresampled ($tmp, $src, 0, 0, $imageX, $imageY, $width, $height, $imageWidth, $imageHeight);
          $result = imagepng($tmp, $thumb, 9);
        } else {
          $src = imagecreatefromjpeg($path);
          $tmp = imagecreatetruecolor($width, $height);

          $bgc = imagecolorallocate($tmp, 255, 255, 255);
              $tc  = imagecolorallocate($tmp, 0, 0, 0);
              imagefilledrectangle($tmp, 0, 0, 150, 30, $bgc);

          imagecopyresampled($tmp, $src, 0, 0, $imageX, $imageY, $width, $height, $imageWidth, $imageHeight);
          $result = imagejpeg($tmp, $thumb, 90);
        }

        imagedestroy($src);
        imagedestroy($tmp);

        if ($result) {
          return $thumb;
        }
        return false;
    }

    protected function _checkSize($size) {
        if (isset ($this->sizes [$size])) {
            return $this->sizes [$size];
        } else {
            $this->app->abort (404);
        }
    }

    protected function _getMimeType($extension) {
        foreach ($this->mimeTypes as $mimeType => $extensions) {
            if (in_array($extension, $extensions)) {
                return $mimeType;
            }
        }
    }

    protected function _returnImage($path) {
        $modTime = filemtime($path);
        $mimeType = $this->_getMimeType(pathinfo($path, PATHINFO_EXTENSION));

        $response = new Response();
        $response->headers->set ('Last-Modified', gmdate('D, d M Y H:i:s', $modTime).' GMT');
        $response->headers->set ('Content-Type', $mimeType);
        $response->setContent (file_get_contents($path));
        $response->send ();
        exit;
    }

    public function delete($filekey, $section) {
        $path = $this->upload_path . $section . '/' . $filekey;
        if (!is_file($path)) return;

        unlink ($path);
        $files = glob($this->upload_path . $section . '/' . pathinfo ($filekey, PATHINFO_FILENAME) . '_*');
        foreach ($files as $filename) unlink ($filename);
    }

    public function getUploadPath() {
        return $this->upload_path;
    }
}