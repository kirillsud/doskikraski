<?php
namespace app\services;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Сервис художников. Позволяет получать информацию о художниках,
 * изменять, добавлять и удалять.
 */
class artistServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['artist_service'] = $app->share(function () use ($app) {
            return new ArtistService ($app);
        });
    }
}

class ArtistService {
    /** @var \Silex\Application */
    private $app;

    /** @var \Doctrine\DBAL\Connection */
    private $db;

    /** @var ImageService */
    private $imageService;
    
    /** @var GalleryService */
    private $galleryService;

    private $imageSection;
    private $lastErrors;
    private $filePath;
    private $table;

    public function __construct (Application $app) {
        $this->app = $app;
        $this->db = $app ['db'];
        $this->imageService = $app ['image_service'];
        $this->galleryService = $app ['gallery_service'];
        $this->imageSection = 'artists';
        $this->filePath = $this->imageService->getUploadPath() . $this->imageSection;
        $this->table = 'artists';
    }

    public function all (array $params = array(), $only_visible = false) {
        $limit = isset($params ['offset']) && isset($params ['limit']) ? "LIMIT {$params ['offset']}, {$params ['limit']}" : "";
        $order = isset($params ['order']) ? ("ORDER BY " . $params ['order']) : "ORDER BY sort_order DESC, timestamp DESC";
        $where = $only_visible ? "WHERE visible = 1" : "";
        $sql = "SELECT * FROM {$this->table} $where " . $order . " ". $limit;
        $data = $this->app['db']->fetchAll ($sql);
        return $data;
    }

    public function total ($only_visible = false) {
        $where = $only_visible ? "WHERE visible = 1" : "";
        $sql = "SELECT count(*) FROM {$this->table} $where";
        $data = $this->app['db']->fetchArray ($sql);
        return (int)$data [0];
    }

    public function get ($id) {
        $sql = "SELECT * FROM {$this->table} WHERE id = ?";
        $data = $this->app['db']->fetchAssoc ($sql, array($id));
        return $data ? $data : false;
    }

    public function getBySlug ($slug) {
        $sql = "SELECT * FROM {$this->table} WHERE slug = ?";
        $data = $this->app['db']->fetchAssoc ($sql, array($slug));
        return $data ? $data : false;
    }

    public function validate (&$data) {
        $this->lastErrors = array ();
        $data ['name'] = strip_tags ($data ['name']);
        $data ['slug'] = mb_strtolower (strip_tags ($data ['slug']), 'utf-8');

//        Картинка не обязательна
//        if (!isset ($data ['id']) && (!isset ($data ['image']) || !$data ['image']['tmp_name'])) {
//            $this->lastErrors ['image'] = 'Без картинки ни как не сохранить';
//            return false;
//        }

        if (isset ($data ['image'])) {
            $extension = null;
            switch (exif_imagetype ($data ['image']['tmp_name'])) {
                case IMAGETYPE_GIF:
                    $extension = 'gif';
                    break;

                case IMAGETYPE_JPEG:
                    $extension = 'jpg';
                    break;

                case IMAGETYPE_PNG:
                    $extension = 'png';
                    break;

                default:
                    $this->lastErrors ['image'] = 'Возможно загрузить изображение только следующих форматов: JPG, PNG, GIF';
                    return false;
            }

            $data ['image']['extension'] = $extension;
        }

        if (mb_strlen ($data ['name'], 'utf8') > 200) {
            $this->lastErrors ['name'] = 'Имя художника не может быть больше 200 символов';
            return false;
        }

        if (mb_strlen ($data ['slug'], 'utf8') > 50) {
            $this->lastErrors ['slug'] = 'Адрес страницы художника не может быть больше 50 символов';
            return false;
        }

        if (!preg_match('#^[a-z_0-9-]+$#', $data ['slug'])) {
            $this->lastErrors ['slug'] = 'Адрес страницы художника должен состоять только из символов английского алфавита, цифр и символов _ и -';
            return false;
        }

        if (mb_strlen ($data ['about'], 'utf8') > 2000) {
            $this->lastErrors ['about'] = 'Информация о художнике не может быть больше 2000 символов';
            return false;
        }

        $data ['visible'] = isset ($data ['visible']) ? 1 : 0;

        return empty ($this->lastErrors);
    }

    public function save ($data, $object = null) {
        if (isset ($data ['image'])) {
            if ($object && $object ['image_key']) {
                $this->imageService->delete ($object ['image_key'], $this->imageSection);
            }

            // TODO: Заменить методом загрузки из сервиса картинок
            if (!is_dir ($this->filePath)) mkdir ($this->filePath, 0775, true);
            $imageKey = ($object && $object ['image_key']) ? $object ['image_key'] :
                (md5 (time ()) . '.' . $data ['image']['extension']);
            move_uploaded_file ($data ['image']['tmp_name'], $this->filePath . '/' . $imageKey );

            $data ['image_key'] = $imageKey;
            unset ($data ['image']);
        }

        if (isset ($data ['id'])) {
            $this->db->update ($this->table, $data, array ('id' => $data ['id']));
            return $data ['id'];
        } else {
            // получаем максимальный номер порядка артистов
            $last_sort_order = $this->db->fetchColumn ("SELECT MAX(`sort_order`) FROM {$this->table}");
            $data ['sort_order'] = $last_sort_order + 1;

            $this->db->insert ($this->table, $data);
            $data = $this->db->fetchArray ('SELECT LAST_INSERT_ID ()');
            return (int) $data [0];
        }
    }

    public function delete ($id) {
        if (false === $data = $this->get ($id)) return false;
        $imageKey = $data ['image_key'];

        $pictures = $this->galleryService->all ($id);
        foreach ($pictures as $picture) $this->galleryService->delete ($picture ['id']);

        if ($this->db->delete ($this->table, array ('id' => (int)$id)) === 0)
            return false;

        $this->imageService->delete ($imageKey, $this->imageSection);
        return true;
    }

    public function deleteImage ($id) {
        if (false === $data = $this->get ($id)) return false;
        $imageKey = $data ['image_key'];
        $data ['image_key'] = null;

        if (!$this->save ($data)) return false;
        $this->imageService->delete ($imageKey, $this->imageSection);
        return true;
    }

    public function up ($id) {
        if (false === $data1 = $this->get ($id)) return false;

        $sql = "SELECT * FROM {$this->table} WHERE sort_order > ? ORDER BY sort_order ASC LIMIT 1";
        if (!$data2 = $this->app['db']->fetchAssoc ($sql, array($data1 ['sort_order']))) return true;

        list ($data1 ['sort_order'], $data2 ['sort_order']) = array ($data2 ['sort_order'], $data1 ['sort_order']);
        $this->save($data1);
        $this->save($data2);
        return true;
    }

    public function down ($id) {
        if (false === $data1 = $this->get ($id)) return false;

        $sql = "SELECT * FROM {$this->table} WHERE sort_order < ? ORDER BY sort_order DESC LIMIT 1";
        if (!$data2 = $this->app['db']->fetchAssoc ($sql, array($data1 ['sort_order']))) return true;

        list ($data1 ['sort_order'], $data2 ['sort_order']) = array ($data2 ['sort_order'], $data1 ['sort_order']);
        $this->save($data1);
        $this->save($data2);
        return true;
    }

    public function errors () {
        return $this->lastErrors;
    }
}
