<?php
namespace app\services;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Сервис спонсоров. Позволяет получать информацию о спонсорах,
 * изменять, добавлять и удалять.
 */
class sponsorServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['sponsor_service'] = $app->share(function () use ($app) {
            return new SponsorService ($app);
        });
    }
}

class SponsorService {
    /** @var \Silex\Application */
    private $app;
    private $last_errors;

    protected $file_path;
    protected $table;

    public function __construct (Application $app) {
        $this->app = $app;
        $this->file_path = DOCROOT . '/media/uploads/sponsors';
        $this->table = 'sponsors';
    }

    public function all (array $params = array(), $only_visible = false) {
        $limit = $params ? "LIMIT {$params ['offset']}, {$params ['limit']}" : "";
        $where = $only_visible ? "WHERE visible = 1" : "";
        $sql = "SELECT * FROM {$this->table} $where ORDER BY sort_order DESC, timestamp DESC " . $limit;
        $data = $this->app['db']->fetchAll ($sql);
        return $data;
    }

    public function total ($only_visible = false) {
        $where = $only_visible ? "WHERE visible = 1" : "";
        $sql = "SELECT count(*) FROM {$this->table} $where";
        $data = $this->app['db']->fetchArray ($sql);
        return (int)$data [0];
    }

    public function get ($id) {
        $sql = "SELECT * FROM {$this->table} WHERE id = ?";
        $data = $this->app['db']->fetchAssoc ($sql, array($id));
        return $data ? $data : false;
    }

    public function getBySlug ($url) {
        $sql = "SELECT * FROM {$this->table} WHERE url = ?";
        $data = $this->app['db']->fetchAssoc ($sql, array($url));
        return $data ? $data : false;
    }

    public function validate (&$data) {
        $this->last_errors = array ();
        $data ['name'] = strip_tags ($data ['name']);
        $data ['url'] = mb_strtolower (strip_tags ($data ['url']), 'utf-8');
        $data ['phone'] = strip_tags ($data ['phone']);
        $data ['address'] = strip_tags ($data ['address']);
        $data ['working_time'] = strip_tags ($data ['working_time']);

        if (!isset ($data ['id']) && (!isset ($data ['image']) || !$data ['image']['tmp_name'])) {
            $this->last_errors ['image'] = 'Без картинки ни как не сохранить';
            return false;
        }

        if (isset ($data ['image'])) {
            $extension = null;
            switch (exif_imagetype ($data ['image']['tmp_name'])) {
                case IMAGETYPE_GIF:
                    $extension = 'gif';
                    break;

                case IMAGETYPE_JPEG:
                    $extension = 'jpg';
                    break;

                case IMAGETYPE_PNG:
                    $extension = 'png';
                    break;

                default:
                    $this->last_errors ['image'] = 'Возможно загрузить изображение только следующих форматов: JPG, PNG, GIF';
                    return false;
            }

            $data ['image']['extension'] = $extension;
        }

        if (mb_strlen ($data ['name'], 'utf8') > 200) {
            $this->last_errors ['name'] = 'Имя спонсора не может быть больше 200 символов';
            return false;
        }

        if (mb_strlen ($data ['phone'], 'utf8') > 15) {
            $this->last_errors ['phone'] = 'Телефон не может быть больше 15 символов';
            return false;
        }

        if (mb_strlen ($data ['address'], 'utf8') > 200) {
            $this->last_errors ['address'] = 'Адрес не может быть больше 200 символов';
            return false;
        }

        if (mb_strlen ($data ['working_time'], 'utf8') > 100) {
            $this->last_errors ['working_time'] = 'Время работы не может быть больше 100 символов';
            return false;
        }

        if (mb_strlen ($data ['url'], 'utf8') > 100) {
            $this->last_errors ['url'] = 'Адрес сайта спонсора не может быть больше 100 символов';
            return false;
        }

        if (!preg_match('#^[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$#i', $data ['url'])) {
            $this->last_errors ['url'] = 'Адрес сайта спонсора содержит неверные символы';
            return false;
        }

        if (mb_strlen ($data ['about'], 'utf8') > 5000) {
            $this->last_errors ['about'] = 'Информация о спонсоре не может быть больше 5000 символов';
            return false;
        }

        $data ['visible'] = isset ($data ['visible']) ? 1 : 0;

        return empty ($this->last_errors);
    }

    public function save ($data, $object = null) {
        if (isset ($data ['image'])) {
            $file_name = ($object && $object ['image_key']) ? $object ['image_key'] :
                (md5 (time ()) . '.' . $data ['image']['extension']);

            if (!is_dir ($this->file_path)) mkdir ($this->file_path, 0775, true);

            $files = glob($this->file_path . '/' . pathinfo ($file_name, PATHINFO_FILENAME) . '_*');
            foreach ($files as $filename) unlink ($filename);

            move_uploaded_file ($data ['image']['tmp_name'], $this->file_path . '/' . $file_name );

            $data ['image_key'] = $file_name;
            unset ($data ['image']);
        }

        if (isset ($data ['id'])) {
            $this->app ['db']->update ($this->table, $data, array ('id' => $data ['id']));
            return $data ['id'];
        } else {
            // получаем максимальный номер порядка
            $last_sort_order = $this->app ['db']->fetchColumn ("SELECT MAX(`sort_order`) FROM {$this->table}");
            $data ['sort_order'] = $last_sort_order + 1;

            $this->app ['db']->insert ($this->table, $data);
            $data = $this->app ['db']->fetchArray ('SELECT LAST_INSERT_ID ()');
            return (int) $data [0];
        }
    }

    public function delete ($id) {
        $pictures = $this->app ['gallery_service']->all ($id);
        foreach ($pictures as $picture) $this->app ['gallery_service']->delete ($picture ['id']);

        return $this->app ['db']->delete ($this->table, array ('id' => (int)$id)) > 0;
    }

    public function up ($id) {
        if (false === $data1 = $this->get ($id)) return false;

        $sql = "SELECT * FROM {$this->table} WHERE sort_order > ? ORDER BY sort_order ASC LIMIT 1";
        if (!$data2 = $this->app['db']->fetchAssoc ($sql, array($data1 ['sort_order']))) return true;

        list ($data1 ['sort_order'], $data2 ['sort_order']) = array ($data2 ['sort_order'], $data1 ['sort_order']);
        $this->save($data1);
        $this->save($data2);
        return true;
    }

    public function down ($id) {
        if (false === $data1 = $this->get ($id)) return false;

        $sql = "SELECT * FROM {$this->table} WHERE sort_order < ? ORDER BY sort_order DESC LIMIT 1";
        if (!$data2 = $this->app['db']->fetchAssoc ($sql, array($data1 ['sort_order']))) return true;

        list ($data1 ['sort_order'], $data2 ['sort_order']) = array ($data2 ['sort_order'], $data1 ['sort_order']);
        $this->save($data1);
        $this->save($data2);
        return true;
    }

    public function errors () {
        return $this->last_errors;
    }
}
