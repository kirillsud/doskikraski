function clearCatalogDescription ($catalog, $desc)
{
    $desc.html ('');
}

function processCatalogDescription ($catalog, $desc, data)
{
    $desc.html (data);
}

$(function () {
    $(window).resize (reset_body_position);
    reset_body_position();
    init_main_slider ();
    init_catalog ();
});

function reset_body_position () {
    $window = $(window);
    $body = $("#body-wrapper");
    var body_width  = 900;
    var body_height = 680;
    var body_margin = 15;
    var body_top_min = 100;
    var body_bottom_min = 33;
    var body_top    = Math.max(body_top_min, ($window.height () - body_height) / 2);
    var body_bottom = Math.max(body_bottom_min, $window.height () - body_height - body_top);

    $('#header').css ('padding-top', body_top - $('#header').height() - 10);
    $('#footer').css ('height', body_bottom - 13);
    $('#content').css ({
        'padding-top': body_top + body_margin,
        'padding-bottom': body_bottom + body_margin
    });
    $('#left-side, #right-side').height($('#main-feed').outerHeight());
}

function init_main_slider () {
    $slides = $('#main-slider > a');
    if (!$slides.size()) return;

    $current_inx = 0;
    $slides.not (':eq(' + $current_inx + ')').hide ();
    setInterval (change_main_slide, 5000);

    function change_main_slide () {
        do {
            $new_inx = Math.ceil (((Math.random() + 0.001) * $slides.size()) - 0.999);
        } while ($new_inx == $current_inx);

        $slides.eq ($current_inx).fadeOut (2000);
        $slides.eq ($new_inx).fadeIn (2000);
        $current_inx = $new_inx;
    }
}

function init_catalog () {
    $catalog_grid = $("#catalog_grid");
    if (!$catalog_grid.size()) return;

    var model = $catalog_grid.data ("model");
    $img_loader = $catalog_grid.find ("#catalog_big > .loader");

    $thumbs = $catalog_grid.children ('#catalog_thumbs');
    $big = $catalog_grid.find ("#catalog_big > img");
    $big.load (function () { $img_loader.hide (); });
    $desc = $('#catalog_description');

    // отображения большой картинки и загрузка описания объекта
    $thumbs.children ().click (function () {
        if ($big.attr ("src") != $(this).data ("big-image")) {
            $img_loader.show ();
            $big.attr ("src", $(this).data ("big-image"));
        }

        window.clearCatalogDescription ($catalog_grid, $desc);

        width = $desc.parent().width() - $desc.parent().children ('#paginator').width () - 30;
        $desc.addClass('loading').width (width).show ();

        $.get ('/ajax/' + model + '/' + $(this).data ("id"), function (data) {
            $desc.removeClass ('loading');
            window.processCatalogDescription ($catalog_grid, $desc, data);
        });

        return true;
    });

    // загрузка картинки и описания образа после загрузки страницы
    lookPattern = new RegExp ('^#' + model + '(\\d+)$');
    match = lookPattern.exec(location.hash);
    look_id = match ? match[1] : $thumbs.children (':first').data ("id");
    if (look_id) {
        $look = $catalog_grid.find ('#catalog_thumbs > a[data-id=' + look_id + ']');
        $look.mouseover ().click ();
    }
}

function showRightPopup(popupId) {
    var popup = $('#' + popupId);

    if (!popup.length) return;

    if (popup.is(':visible')) {
        popup.parent().animate({left: '0'}, 500, function() { $('#' + popupId).hide() });
    } else {
        popup.parent().animate({left: '-220px'}, 500);
        $('#' + popupId).show();
    }
}