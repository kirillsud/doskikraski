$( function () {
    $page_content = $("#mainfeed_content_, #page_content_, #artist_about_, #event_content_");
    if ($page_content.size() && CKEDITOR) $page_content.ckeditor ({
        skin: 'moonocolor',
        toolbar: [
            { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','RemoveFormat','-','Scayt' ] },
            { name: 'insert', items : [ 'Image','MediaEmbed','Uploadcare','Slideshow','-','Table','HorizontalRule','SpecialChar' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript' ] },
            '/',
            { name: 'styles', items : [ 'Format','Font','FontSize' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            { name: 'tools', items : window.preview_url ? [ 'Maximize','Preview','-','About' ] : [ 'Maximize','-','About' ] }
        ],
        contentsCss : '/media/style.css'
    });

    $menu_useritems = $("ul.admin_menu").sortable ({
        items: "li:has(a.userpage)",
        update: function(event, ui) {
            var order = [];
            for (var i=0; i < $menu_useritems.size(); i++) {
                $item = $ ($menu_useritems [i]);
                order [$item.index()] = $item.children ('a').data ('id');
            }
            $.post ('/admin/userpage/order', {'order': order});
        }
    }).children ("li:has(a.userpage)");

    $('.order_action_up').click (moveUp);
    $('.order_action_down').click (moveDown);

    if ((field_artist_name = $('#artist_name_')).length) {
        field_artist_name.keyup (function () {
            var new_val = URLify (this.value, 50);
            var slug = $('#artist_slug_');

            if (this.oldvalue == slug.val()) slug.val (new_val);
            this.oldvalue = new_val;
        })[0].oldvalue = '';
    }
});

function URLify(s, num_chars) {
    var s = ru2en.translit(s);
    // changes, e.g., "Petty theft" to "petty_theft"
    // remove all these words from the string before urlifying
    removelist = ["a", "an", "as", "at", "before", "but", "by", "for", "from",
        "is", "in", "into", "like", "of", "off", "on", "onto", "per",
        "since", "than", "the", "this", "that", "to", "up", "via",
        "with"];
    r = new RegExp('\\b(' + removelist.join('|') + ')\\b', 'gi');
    s = s.replace(r, '');
    s = s.replace(/[^-A-Z0-9\s]/gi, '');  // remove unneeded chars
    s = s.replace(/^\s+|\s+$/g, ''); // trim leading/trailing spaces
    s = s.replace(/[-\s]+/g, '-');   // convert spaces to hyphens
    s = s.toLowerCase();             // convert to lowercase
    return s.substring(0, num_chars);// trim to first num_chars chars
}

var ru2en = {
    ru_str : "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя",
    en_str : ['A','B','V','G','D','E','JO','ZH','Z','I','J','K','L','M','N','O','P','R','S','T',
        'U','F','H','C','CH','SH','SHH',String.fromCharCode(35),'I',String.fromCharCode(39),'JE','JU',
        'JA','a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f',
        'h','c','ch','sh','shh',String.fromCharCode(35),'i',String.fromCharCode(39),'je','ju','ja'],
    translit : function(org_str) {
        var tmp_str = "";
        for(var i = 0, l = org_str.length; i < l; i++) {
            var s = org_str.charAt(i), n = this.ru_str.indexOf(s);
            if(n >= 0) { tmp_str += this.en_str[n]; }
            else { tmp_str += s; }
        }
        return tmp_str;
    }
}

function moveUp () {
    $this = $(this).closest('tr');
    if (!$this.is (':first-child')) {
        $.get (this.href);
        $this.fadeOut (function () {
            var tmpClass = $this.attr('class');
            $this.attr ('class', $this.prev().attr('class'));
            $this.prev().attr ('class', tmpClass);
            $this.insertBefore($this.prev()).fadeIn ();
        });
    }
    return false;
}

function moveDown () {
    $this = $(this).closest('tr');
    if (!$this.is (':last-child')) {
        $.get (this.href)
        $this.fadeOut (function () {
            var tmpClass = $this.attr('class');
            $this.attr ('class', $this.next().attr('class'));
            $this.next().attr ('class', tmpClass);
            $this.insertAfter($this.next()).fadeIn ();
        });
    }
    return false;
}

function resizeIframe(iframeID) {
    parent.document.getElementById(iframeID).height = parent.document.getElementById(iframeID).contentDocument['body'].offsetHeight + 20;
}

function pagePreview(url, formId) {
    var createInput = function(name, value)
    {
        var input = document.createElement('INPUT');
        input.type = 'hidden';

        input.name = name;
        input.value = value;
        return input;
    };

    // Init url for preview form action
    url = url || window.preview_url || null;
    if (!url) return;

    // Get the form with data to submit (real form)
    var dataFormId = formId || 'admin_edit_form';
    var dataForm = document.getElementById(dataFormId);
    if (!dataForm || dataForm.tagName != 'FORM') {
        console.error('Form with id = "' + dataFormId + '" not found');
        return;
    }

    // Get the form to submit the data (a custom one, not the real)
    var theForm = document.getElementById('pagePreviewForm');
    if (!theForm) {
        // If it doesn't exist, we create it here
        theForm = document.createElement('FORM');
        theForm.method = 'POST';
        theForm.name = 'pagePreviewForm';
        theForm.id = theForm.name;
        theForm.style.display = 'none';

        // define target for the preview
        theForm.target = 'pagePreview';
        document.body.appendChild( theForm );
    }

    // This sets the default page where the data will be posted.
    theForm.action = typeof url == "function" ? url() : url;

    // Clear previous data
    theForm.innerHTML = '';

    // Set the new content
    /** @var dataForm HTMLFormElement **/
    var fields = dataForm.elements;
    for (var i=0; i<fields.length; i++)
    {
        var field = fields[ i ];

        if (!field.name || !field.id) continue;

        var id = field.id;
        var name = field.name;
        var extraEditor = CKEDITOR.instances[ id ];

        if (field.tagName == 'INPUT' && ['checkbox', 'radio'].indexOf(field.type) >= 0 && !field.checked) {
            continue;
        }

        var value = ( extraEditor ? extraEditor.getData() : field.value );
        theForm.appendChild( createInput( name, value ) );
    }

    // Send the data to the server
    var w = window.open('about:blank', "pagePreview",
        "left=50,top=100,width=" +  (screen.width - 100) + ",height=" + (screen.height - 200));

    if (w) w.focus();

    theForm.submit();
}