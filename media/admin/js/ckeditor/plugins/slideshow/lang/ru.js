﻿/*
Copyright (c) 2003-2013, Cricri042. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
Targeted for "ad-gallery" JavaScript : http://adgallery.codeplex.com/
And "Fancybox" : http://fancyapps.com/fancybox/
*/
CKEDITOR.plugins.setLang( 'slideshow', 'ru', {
	toolbar	: 'Слайд-шоу',
	dialogTitle : 'Слайд-шоу',
	fakeObject : 'Слайд-шоу',
	imgList : 'Список изображений',
	imgTitle : 'Заголовок',
    imgAdd : 'Добавить изображение',
    imgDelete : 'Удалить изображения',
	imgEdit : 'Изменить описание',
	previewMode : 'Предпросмотр',
	imgDesc : 'Описание',
	editSlideShow : 'Изменить слайд-шоу',
	picturesList : "Изображения :",
	insertSlideShow : 'Вставить Слайд-шоу',
	showThumbs : 'Показывать эскизы',
	showTitle : 'Показывать заголовок',
	showControls : "Показывать Старт / Стоп",
	displayTime : 'Время паказа (сек.)',
	transitionTime : 'Плавность перехода (мсек.)',
	autoStart : 'Автостарт',
    pictWidth : 'Ширина (пикс.)',
    pictHeight : 'Высота (писк.)',
	openOnClick : 'Открывать кликом',
	transition : 'Типы переходов',
	tr1 : 'Нет',
	tr2 : 'Растяжимый',
	tr3 : 'Вертикальное слайдшоу',
	tr4 : 'Горизонтальное слайдшоу',
	tr5 : 'Постепенно исчезать'
});
