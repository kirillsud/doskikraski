/**
 * The slideshow dialog definition.
 * Copyright (c) 2003-2013, Cricri042. All rights reserved.
 * Targeted for "ad-gallery" JavaScript : http://adgallery.codeplex.com/
 * And "Fancybox" : http://fancyapps.com/fancybox/
 */
/**
 * Debug : var_dump
 *
 * @var: Var
 * @level: Level max
 *
 */

var IMG_PARAM = {"URL": 0, "TITLE": 1, "ALT": 2, "WIDTH": 3, "HEIGHT": 4};
var pluginPath = CKEDITOR.plugins.get('slideshow').path;
SCRIPT_JQUERY = "https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js";
SCRIPT_ADDGAL = pluginPath + "3rdParty/ad-gallery/jquery.ad-gallery.min.js";
CSS_ADDGAL = pluginPath + "3rdParty/ad-gallery/jquery.ad-gallery.css";
SCRIPT_FANCYBOX = pluginPath + '3rdParty/fancybox2/jquery.fancybox.pack.js?v=2.1.5';
CSS_FANCYBOX = pluginPath + "3rdParty/fancybox2/jquery.fancybox.css?v=2.1.5";


var iFrameItem = function (node) {
    return node.type == CKEDITOR.NODE_ELEMENT && node.is('iframe');
};

Array.prototype.pushUnique = function (item) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][0] == item[0]) return -1;
    }
    this.push(item);
    return this.length - 1;
};

Array.prototype.updateVal = function (item, data) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][0] == item) {
            this[i] = [item, data];
            return true;
        }
    }
    this[i] = [item, data];
    return false;
};

Array.prototype.getVal = function (item) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][0] == item) {
            return this[i][1];
        }
    }
    return null;
};


// Our dialog definition.
CKEDITOR.dialog.add('slideshowDialog', function (editor) {
    var lang = editor.lang.slideshow;

//----------------------------------------------------------------------------------------------------	
// COMBO STUFF	
//----------------------------------------------------------------------------------------------------	
    // Add a new option to a CHKBOX object (combo or list).
    function addOption(combo, optionText, optionValue, documentObject, index) {
        combo = getSelect(combo);
        var oOption;
        if (documentObject)
            oOption = documentObject.createElement("OPTION");
        else
            oOption = document.createElement("OPTION");

        if (combo && oOption && oOption.getName() == 'option') {
            if (CKEDITOR.env.ie) {
                if (!isNaN(parseInt(index, 10)))
                    combo.$.options.add(oOption.$, index);
                else
                    combo.$.options.add(oOption.$);

                oOption.$.innerHTML = optionText.length > 0 ? optionText : '';
                oOption.$.value = optionValue;
            }
            else {
                if (index !== null && index < combo.getChildCount())
                    combo.getChild(index < 0 ? 0 : index).insertBeforeMe(oOption);
                else
                    combo.append(oOption);

                oOption.setText(optionText.length > 0 ? optionText : '');
                oOption.setValue(optionValue);
            }
        } else {
            return false;
        }
        return oOption;
    }

    function removeAllOptions(combo) {
        combo = getSelect(combo);
        while (combo.getChild(0) && combo.getChild(0).remove()) { /*jsl:pass*/
        }
    }

    function getSelectedIndex(combo) {
        combo = getSelect(combo);
        return combo ? combo.$.selectedIndex : -1;
    }

    function setSelectedIndex(combo, index) {
        combo = getSelect(combo);
        if (index < 0)
            return null;
        var count = combo.getChildren().count();
        combo.$.selectedIndex = ( index >= count ) ? ( count - 1 ) : index;
        return combo;
    }

    function getSelect(obj) {
        if (obj && obj.domId && obj.getInputElement().$)
            return  obj.getInputElement();
        else if (obj && obj.$)
            return obj;
        return false;
    }

    function displaySelected(dialog) {
        if (dialog.openCloseStep == true) return;
        var previewCombo = dialog.getContentElement('slideshowinfoid', 'framepreviewid');
        if (previewCombo.isVisible()) {
            previewSlideShow(dialog);
        } else {
            editSelected(dialog);
        }
    }

    function selectFirstIfNotUnique(combo) {
        var dialog = combo.getDialog();
        combo = getSelect(combo);
        var firstSelectedInd = 0;
        for (var i = 0; i < combo.getChildren().count(); i++) {
            var child = combo.getChild(i);
            if (child.$.selected) {
                firstSelectedInd = i;
                break;
            }
        }
        setSelectedIndex(combo, firstSelectedInd);
        displaySelected(dialog);
    }

    function getDialogSelectedIndex(dialog) {
        var combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
        return getSelectedIndex(combo);
    }

//----------------------------------------------------------------------------------------------------	
//----------------------------------------------------------------------------------------------------	
    function getIconStyle(icon) {
        var style = CKEDITOR.skin.getIconStyle(icon);
        if (!style) return "";

        var pattern = /background-position:(-?\d+)(px)? (-?\d+)(px)?/g;
        var match = pattern.exec(style);
        var x = parseInt(match[1]);
        var y = parseInt(match[3]);
        return style.replace(pattern, 'background-repeat:no-repeat;background-position:' + (x + 4) + "px " + (y + 3) + "px");
    }

    function removePlaceHolderImg(dialog) {
        var urlPlaceHolder = CKEDITOR.basePath + 'plugins/slideshow/icons/placeholder.png';
        if ((dialog.imagesList.length == 1) && (dialog.imagesList[0][IMG_PARAM.URL] == urlPlaceHolder)) {
            // Remove the place Holder Image
            var combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
            combo = getSelect(combo);
            var i = 0;
            // Remove image from image Array
            dialog.imagesList.splice(i, 1);
            // Remove image from combo image list
            combo.getChild(i).remove();
        }
    }

    function updateImgList(dialog) {
        removePlaceHolderImg(dialog);
        var preview = dialog.previewImage;
        var url = preview.$.src;
        var ratio = preview.$.width / preview.$.height;
        var w = 50;
        var h = 50;
        if (ratio > 1) {
            h = h / ratio;
        } else {
            w = w * ratio;
        }
        var ind = dialog.imagesList.pushUnique([url, '', '', w.toFixed(0), h.toFixed(0)]);
        if (ind >= 0) {
            addOption(dialog.combo, 'IMG_' + ind + ' : ' + url.substring(url.lastIndexOf('/') + 1), url, dialog.getParentEditor().document);
            // select index 0
            setSelectedIndex(dialog.combo, ind);
            // Update dialog
            displaySelected(dialog);
        }
    }

    function editSelected(dialog) {
        var combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
        var iSelectedIndex = getSelectedIndex(combo);
        var value = dialog.imagesList[iSelectedIndex];

        combo = dialog.getContentElement('slideshowinfoid', 'imgtitleid');
        combo = getSelect(combo);
        combo.setValue(value[1]);
        combo = dialog.getContentElement('slideshowinfoid', 'imgdescid');
        combo = getSelect(combo);
        combo.setValue(value[2]);
        combo = dialog.getContentElement('slideshowinfoid', 'imgpreviewid');
        combo = getSelect(combo);
        //console.log( "VALUE IMG -> " +  value[iSelectedIndex] );
        var imgHtml = '<div style="text-align:center;"> <img src="' + value[0] +
            '" title="' + value[1] +
            '" alt="' + value[2] +
            '" style=" max-height: 200px;  max-width: 350px;' + '"> </div>';
        combo.setHtml(imgHtml);
        var previewCombo = dialog.getContentElement('slideshowinfoid', 'framepreviewid');
        var imgCombo = dialog.getContentElement('slideshowinfoid', 'imgparamsid');
        previewCombo = getSelect(previewCombo);
        previewCombo.hide();
        imgCombo = getSelect(imgCombo);
        imgCombo.show();
    }

    function removeSelected(dialog) {
        var combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
        combo = getSelect(combo);
        var someRemoved = false;
        // Remove all selected options.
        for (var i = combo.getChildren().count() - 1; i >= 0; i--) {
            if (combo.getChild(i).$.selected) {
                // Remove image from image Array
                dialog.imagesList.splice(i, 1);
                // Remove image from combo image list
                combo.getChild(i).remove();
                someRemoved = true;
            }
        }
        if (someRemoved) {
            if (dialog.imagesList.length == 0) {
                var url = CKEDITOR.basePath + 'plugins/slideshow/icons/placeholder.png';
                addOption(combo, 'IMG_0' + ' : ' + url.substring(url.lastIndexOf('/') + 1), url, dialog.getParentEditor().document);
                dialog.imagesList.pushUnique([url, lang.imgTitle, lang.imgDesc, '50', '50']);
            }
            // select index 0
            setSelectedIndex(combo, 0);
            // Update dialog
            displaySelected(dialog);
        }
    }

    // To automatically get the dimensions of the poster image
    var onImgLoadEvent = function () {
        // Image is ready.
        var preview = this.previewImage;
        preview.removeListener('load', onImgLoadEvent);
        preview.removeListener('error', onImgLoadErrorEvent);
        preview.removeListener('abort', onImgLoadErrorEvent);
        //console.log( "previewImage -> " + preview );
        updateImgList(this);
    };

    var onImgLoadErrorEvent = function () {
        // Error. Image is not loaded.
        var preview = this.previewImage;
        preview.removeListener('load', onImgLoadEvent);
        preview.removeListener('error', onImgLoadErrorEvent);
        preview.removeListener('abort', onImgLoadErrorEvent);
    };

    function updateTitle(dialog, val) {
        dialog.imagesList[getDialogSelectedIndex(dialog)][IMG_PARAM.TITLE] = val;
        editSelected(dialog);
    }

    function updateDescription(dialog, val) {
        dialog.imagesList[getDialogSelectedIndex(dialog)][IMG_PARAM.ALT] = val;
        editSelected(dialog);
    }

    function previewSlideShow(dialog) {
        var previewCombo = dialog.getContentElement('slideshowinfoid', 'framepreviewid');
        var imgCombo = dialog.getContentElement('slideshowinfoid', 'imgparamsid');
        imgCombo = getSelect(imgCombo);
        imgCombo.hide();
        previewCombo = getSelect(previewCombo);
        previewCombo.show();
        updateFramePreview(dialog);
    }

    function feedFrame(frame, data) {
        frame.open();
        frame.writeln(data);
        frame.close();
    }

    function updateFramePreview(dialog) {
        var width = 436;
        var height = 300;
        if (dialog.imagesList.length == 0) return;
        var combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
        var iSelectedIndex = getSelectedIndex(combo);
        if (iSelectedIndex < 0) iSelectedIndex = 0;
        combo = dialog.getContentElement('slideshowinfoid', 'framepreviewid');
        var strVar = "";
        strVar += "<head>";

        strVar += '<script src="' + SCRIPT_JQUERY + '" type="text/javascript"></script>';
        strVar += "<script type=\"text\/javascript\" src=\"" + SCRIPT_ADDGAL + "\"><\/script>";
        strVar += "<link rel=\"stylesheet\" type=\"text\/css\" href=\"" + CSS_ADDGAL + "\" \/>";
        if (dialog.params.getVal('openOnClickId') == true) {
            strVar += "<link rel=\"stylesheet\" type=\"text\/css\" href=\"" + CSS_FANCYBOX + "\" \/>";
            strVar += "<script type=\"text\/javascript\" src=\"" + SCRIPT_FANCYBOX + "\"><\/script>";
            strVar += "<script type=\"text\/javascript\">";
            strVar += createScriptFancyBoxRun(dialog);
            strVar += "<\/script>";
        }

        strVar += "<script type=\"text\/javascript\">";
        strVar += createScriptAdGalleryRun(dialog, iSelectedIndex, width, height);
        strVar += "<\/script>";

        strVar += "<\/head>";
        strVar += "<body>";
        var domGallery = createDOMdGalleryRun(dialog);
        strVar += domGallery.getOuterHtml();
        strVar += "<\/body>";
        strVar += "";

        combo = getSelect(combo);
        var theFrame = combo.getFirst(iFrameItem);
        if (theFrame) theFrame.remove();
        var ifr = null;

        var w = width + 60;
        var iframe = CKEDITOR.dom.element.createFromHtml('<iframe' +
            ' style="width:' + w + 'px;height:' + height + 'px;background:azure; "' +
            ' class="cke_pasteframe"' +
            ' frameborder="10" ' +
            ' allowTransparency="false"' +
//				' src="' + 'data:text/html;charset=utf-8,' +  strVar + '"' +
            ' role="region"' +
            ' scrolling="no"' +
            '></iframe>');

        iframe.setAttribute('name', 'totoFrame');
        iframe.setAttribute('id', 'totoFrame');
        iframe.on('load', function () {
            if (ifr != null) return;
            ifr = this.$;
            var iframeDoc;
            if (ifr.contentDocument)
                iframeDoc = ifr.contentDocument;
            else if (ifr.contentWindow)
                iframeDoc = ifr.contentWindow.document;
            if (iframeDoc) {
                // Put the content in the iframe
                feedFrame(iframeDoc, strVar);
            } else {
                //just in case of browsers that don't support the above 3 properties.
                //fortunately we don't come across such case so far.
                alert('Cannot inject dynamic contents into iframe.');
            }
        });
        combo.append(iframe);
    }

    function initImgListFromDOM(dialog, slideShowContainer) {
        var i, image, src;
        var imgW, imgH;
        var arr = slideShowContainer.$.getElementsByTagName("img");
        dialog.combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
        for (i = 0; i < arr.length; i++) {
            image = arr[i];
            src = image.src;
            // IE Seems sometime to return 0 !!, So natural Width and Height seems OK
            // If not just pput 50, Not as good but not so bad !!
            imgW = image.width;
            if (imgW == 0) imgW = image.naturalWidth;
            if (imgW == 0) {
                imgW = 50;
                imgH = 50;
            } else {
                imgH = image.height;
                if (imgH == 0) imgH = image.naturalHeight;
                if (imgH == 0) {
                    imgW = 50;
                    imgH = 50;
                }
            }
            var ratio = imgW / imgH;
            var w = 50;
            var h = 50;
            if (ratio > 1) {
                h = h / ratio;
            } else {
                w = w * ratio;
            }
            var ind = dialog.imagesList.pushUnique([src, image.title, image.alt, w, h]);
            if (ind >= 0) {
                addOption(dialog.combo, 'IMG_' + ind + ' : ' + src.substring(src.lastIndexOf('/') + 1), src, dialog.getParentEditor().document);
            }
        }
        // select index 0
        setSelectedIndex(dialog.combo, 0);
        // Update dialog
        displaySelected(dialog);
    }

    function initImgListFromFresh(dialog) {
        dialog.combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
        var url = CKEDITOR.basePath + 'plugins/slideshow/icons/placeholder.png';
        addOption(dialog.combo, 'IMG_0' + ' : ' + url.substring(url.lastIndexOf('/') + 1), url, dialog.getParentEditor().document);
        dialog.imagesList.pushUnique([url, lang.imgTitle, lang.imgDesc, '50', '50']);
        // select index 0
        setSelectedIndex(dialog.combo, 0);
        // Update dialog
        displaySelected(dialog);
    }

    function loadValue() {
        var dialog = this.getDialog();
        if (dialog.newSlideShowMode) {
            // New fresh SlideShow so let's put dom data attributes from dialog default values
            dialog.slideshowDOM.setAttribute('data-' + this.id, this.getValue());
            switch (this.type) {
                case 'checkbox':
                    break;
                case 'text':
                    break;
                case 'select':
                    break;
                default:
            }
        } else {
            // Loaded SlideShow, so update Dialog values from DOM data attributes

            switch (this.type) {
                case 'checkbox':
                    this.setValue(dialog.slideshowDOM.getAttribute('data-' + this.id) == 'true');
                    break;
                case 'text':
                    this.setValue(dialog.slideshowDOM.getAttribute('data-' + this.id));
                    break;
                case 'select':
                    this.setValue(dialog.slideshowDOM.getAttribute('data-' + this.id));
                    break;
                default:
            }
        }
    }

    function commitValue() {
        var dialog = this.getDialog();
        dialog.params.updateVal(this.id, this.getValue());
        switch (this.type) {
            case 'checkbox':
                break;
            case 'text':
                break;
            case 'select':
                break;
            default:
        }
        displaySelected(dialog);
    }

    function cleanAll(dialog) {
        if (dialog.previewImage) {
            dialog.previewImage.removeListener('load', onImgLoadEvent);
            dialog.previewImage.removeListener('error', onImgLoadErrorEvent);
            dialog.previewImage.removeListener('abort', onImgLoadErrorEvent);
            dialog.previewImage.remove();
            dialog.previewImage = null;		// Dialog is closed.
        }
        dialog.imagesList = null;
        dialog.params = null;
        dialog.slideshowDOM = null;

        var combo = dialog.getContentElement('slideshowinfoid', 'imglistitemsid');
        removeAllOptions(combo);
        dialog.openCloseStep = false;
    }

    function randomChars(len) {
        var chars = '';
        while (chars.length < len) {
            chars += Math.random().toString(36).substring(2);
        }
        // Remove unnecessary additional characters.
        return chars.substring(0, len);
    }

    var numbering = function (id) {
        //return CKEDITOR.tools.getNextId() + '_' + id;
        return 'cke_' + randomChars(8) + '_' + id;
    };

    function getImagesContainerBlock(dom) {
        var obj = dom.getElementsByTag("ul");
        if (obj == null) return null;
        if (obj.count() == 1) {
            return obj.getItem(0);
        }
        return null;
    }

    function createScriptAdGalleryRun(dialog, iSelectedIndex, width, height) {
        var slideshowId = dialog.params.getVal('slideshowid');
        var galleryId = 'ad-gallery_' + slideshowId;
        var strVar = '';
        var strHook = '';
        strVar += "$(function() {";
        if (width == 0) width = parseInt(dialog.params.getVal('pictWidthId'));
        if (height == 0) height = width == 900 ? 600 : 430;
        if (dialog.params.getVal('showTitleId') == false) {
            strHook = ",  hooks: { displayDescription: function(image) {}}";
        }
        var params = "loader_image: '" + pluginPath + "3rdParty/ad-gallery/loader.gif'," +
            " width:" + width + ", height:" + height +
            ", start_at_index: " + iSelectedIndex +
            ", animation_speed: 500" + strHook +
            ", update_window_hash: false, effect: 'slide-hori',";
        var slideShowParams = " slideshow: { enable: true, autostart: false, speed: 0 },";
        strVar += "   var galleries = $('#" + galleryId + "').adGallery({" + params + slideShowParams + "});";
        strVar += "});";
        return strVar;
    }

    function createScriptFancyBoxRun(dialog) {
        var slideshowId = dialog.params.getVal('slideshowid');
        var galleryId = 'ad-gallery_' + slideshowId;
        var str = '';
//		str +=  "$(document).ready(function() {";
        str += "$(function() {";
        str += "$(\"#" + galleryId + "\").on(\"click\",\".ad-image\",function(){";
        str += "var imgObj =$(this).find(\"img\");";
        str += "var isrc=imgObj.attr(\"src\");";
        str += "var ititle=null;";
        str += "var idesc=null;";
        str += "var iname=isrc.split('/');";
        str += "iname=iname[iname.length-1];";
        str += "var imgdescid=$(this).find(\".ad-image-description\");";
        str += "if(imgdescid){";
        str += "ititle=$(this).find(\".ad-description-title\");";
        str += "if(ititle)ititle=ititle.text();";
        str += "if(ititle!='')ititle='<big>'+ititle+'</big>';";
        str += "idesc=$(this).find(\"span\");";
        str += "if(idesc)idesc=idesc.text();";
        str += "if(idesc!=''){";
        str += "if(ititle!='')ititle=ititle+'<br>';";
        str += "idesc='<i>'+idesc+'</i>';";
        str += "}";
        str += "}";
        str += "$.fancybox.open({";
        str += "href:isrc,";
        str += "beforeLoad:function(){";
        str += "this.title=ititle+idesc;";
        str += "},";
        str += "});";
        str += "});";
        str += "});";
//		str += "});";
        //console.log(str);
        return str;
    }

    function feedUlWithImages(dialog, ulObj) {
        for (var i = 0; i < dialog.imagesList.length; i++) {
            var liObj = ulObj.append('li');
            liObj.setAttribute('contenteditable', 'false');
            var aObj = liObj.append('a');
            aObj.setAttribute('href', dialog.imagesList[i][IMG_PARAM.URL]);
            aObj.setAttribute('contenteditable', 'false');
            var newImgDOM = aObj.append('img');
            newImgDOM.setAttribute('src', dialog.imagesList[i][IMG_PARAM.URL]);
            newImgDOM.setAttribute('title', dialog.imagesList[i][IMG_PARAM.TITLE]);
            newImgDOM.setAttribute('alt', dialog.imagesList[i][IMG_PARAM.ALT]);
            newImgDOM.setAttribute('contenteditable', 'false');
            newImgDOM.setAttribute('width', dialog.imagesList[i][IMG_PARAM.WIDTH]);
            newImgDOM.setAttribute('height', dialog.imagesList[i][IMG_PARAM.HEIGHT]);
        }
    }

    function createDOMdGalleryRun(dialog) {
        var slideshowId = dialog.params.getVal('slideshowid');
        var galleryId = 'ad-gallery_' + slideshowId;
        var displayThumbs = 'display: none;';
        var displayControls = 'display: none;';

        var slideshowDOM = editor.document.createElement('div');
        slideshowDOM.setAttribute('id', slideshowId);
        slideshowDOM.setAttribute('class', 'slideshowPlugin');
        slideshowDOM.setAttribute('contenteditable', 'false');

        var galleryDiv = slideshowDOM.append('div');
        galleryDiv.setAttribute('class', 'ad-gallery');
        galleryDiv.setAttribute('contenteditable', 'false');
        galleryDiv.setAttribute('id', galleryId);

        var wrapperObj = galleryDiv.append('div');
        wrapperObj.setAttribute('class', 'ad-image-wrapper');
        wrapperObj.setAttribute('contenteditable', 'false');

        var controlObj = galleryDiv.append('div');
        controlObj.setAttribute('class', 'ad-controls');
        controlObj.setAttribute('contenteditable', 'false');
        controlObj.setAttribute('style', displayControls);

        var navObj = galleryDiv.append('div');
        navObj.setAttribute('class', 'ad-nav');
        navObj.setAttribute('style', displayThumbs);
        navObj.setAttribute('contenteditable', 'false');

        var thumbsObj = navObj.append('div');
        thumbsObj.setAttribute('class', 'ad-thumbs');
        thumbsObj.setAttribute('contenteditable', 'false');

        var ulObj = thumbsObj.append('ul');
        ulObj.setAttribute('class', 'ad-thumb-list');
        ulObj.setAttribute('contenteditable', 'false');

        feedUlWithImages(dialog, ulObj);
        return slideshowDOM;
    }

    function ClickOkBtn(dialog) {
        var extraStyles = {},
            extraAttributes = {};

        dialog.openCloseStep = true;

        // Invoke the commit methods of all dialog elements, so the dialog.params array get Updated.
        dialog.commitContent(dialog);

        // Create a new DOM
        var slideshowDOM = createDOMdGalleryRun(dialog);

        // Add data tags to dom
        for (var i = 0; i < dialog.params.length; i++) {
            slideshowDOM.data(dialog.params[i][0], dialog.params[i][1]);
        }

        // Add javascript for ""ad-gallery"
        // Be sure the path is correct and file is available !!
        var scriptAdGallery = CKEDITOR.document.createElement('script', {
            attributes: {
                type: 'text/javascript',
                src: SCRIPT_ADDGAL
            }
        });
        slideshowDOM.append(scriptAdGallery);

        if (dialog.params.getVal('openOnClickId') == true) {
            // Dynamically add CSS for "fancyBox"
            // Be sure the path is correct and file is available !!
            var scriptFancyBoxCss = CKEDITOR.document.createElement('script', {
                attributes: {
                    type: 'text/javascript'
                }
            });
            scriptFancyBoxCss.setText("$('head').append('<link rel=\"stylesheet\" href=\"" + CSS_FANCYBOX + "\" type=\"text/css\" />');");
            slideshowDOM.append(scriptFancyBoxCss);

            // Add javascript for ""fancyBox"
            // Be sure the path is correct and file is available !!
            var scriptFancyBox = CKEDITOR.document.createElement('script', {
                attributes: {
                    type: 'text/javascript',
                    src: SCRIPT_FANCYBOX
                }
            });
            slideshowDOM.append(scriptFancyBox);

            // Add RUN javascript for "fancybox"
            var scriptFancyboxRun = CKEDITOR.document.createElement('script', {
                attributes: {
                    type: 'text/javascript'
                }
            });
            scriptFancyboxRun.setText(createScriptFancyBoxRun(dialog));
            slideshowDOM.append(scriptFancyboxRun);
        }

        // Dynamically add CSS for "ad-gallery"
        // Be sure the path is correct and file is available !!
        var scriptAdGalleryCss = CKEDITOR.document.createElement('script', {
            attributes: {
                type: 'text/javascript'
            }
        });
        scriptAdGalleryCss.setText("$('head').append('<link rel=\"stylesheet\" href=\"" + CSS_ADDGAL + "\" type=\"text/css\" />');");
        slideshowDOM.append(scriptAdGalleryCss);

        // Add RUN javascript for "ad-Gallery"
        var scriptAdGalleryRun = CKEDITOR.document.createElement('script', {
            attributes: {
                type: 'text/javascript'
            }
        });
        scriptAdGalleryRun.setText(createScriptAdGalleryRun(dialog, 0, 0, 0));
        slideshowDOM.append(scriptAdGalleryRun);

        if (dialog.imagesList.length) {
            extraStyles.backgroundImage = 'url("' + dialog.imagesList[0][IMG_PARAM.URL] + '")';
        }

        var width = parseInt(slideshowDOM.getAttribute('data-pictwidthid') || 900);
        var height = width == 900 ? 600 : 430;

        extraStyles.backgroundSize = '50%';
        extraStyles.display = 'block';
        extraStyles.width = width + 'px';
        extraStyles.height = height + 'px';
        extraStyles.margin = 'auto';

        // Create a new Fake Image
        var newFakeImage = editor.createFakeElement(slideshowDOM, 'cke_slideShow', 'slideShow', false);
        newFakeImage.setAttributes(extraAttributes);
        newFakeImage.setStyles(extraStyles);

        if (dialog.fakeImage) {
            newFakeImage.replace(dialog.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
        }
        else {
            editor.insertElement(newFakeImage);
        }

        cleanAll(dialog);
        dialog.hide();
    }

    return {
        // Basic properties of the dialog window: title, minimum size.
        title: lang.dialogTitle,
        width: 500,
        height: 600,
        resizable: CKEDITOR.DIALOG_RESIZE_NONE,
        allowedContent: 'div(slideshowPlugin)',
        buttons: [
            CKEDITOR.dialog.okButton(editor, {
                label: 'OkCK',
                style: 'display:none;'
            }),
            CKEDITOR.dialog.cancelButton,

            {
                id: 'myokbtnid',
                type: 'button',
                label: 'OK',
                title: 'Button description',
                style: 'color:white;background:blue;',
                accessKey: 'C',
                disabled: false,
                onClick: function () {
                    // code on click
                    ClickOkBtn(this.getDialog());
                }
            }
        ],

        // Dialog window contents definition.
        contents: [
            {
                // Definition of the Basic Settings dialog (page).
                id: 'slideshowinfoid',
                label: 'Basic Settings',
                align: 'center',
                // The tab contents.
                elements: [
                    {
                        type: 'text',
                        id: 'id',
                        style: 'display:none;',
                        onLoad: function () {
                            this.getInputElement().setAttribute('readOnly', true);
                        }
                    },
                    {
                        type: 'text',
                        id: 'txturlid',
                        style: 'display:none;',
                        label: lang.imgList,
                        onChange: function () {
                            var dialog = this.getDialog(),
                                newUrl = this.getValue();
                            if (newUrl.length > 0) { //Prevent from load before onShow
                                var preview = dialog.previewImage;
                                preview.on('load', onImgLoadEvent, dialog);
                                preview.on('error', onImgLoadErrorEvent, dialog);
                                preview.on('abort', onImgLoadErrorEvent, dialog);
                                preview.setAttribute('src', newUrl);
                            }
                        }
                    },
                    {
                        type: 'vbox',
                        align: 'center',
                        children: [
                            {
                                type: 'html',
                                align: 'center',
                                id: 'framepreviewtitleid',
                                html: lang.previewMode
                            },
                            {
                                type: 'html',
                                id: 'framepreviewid',
                                align: 'center',
                                style: 'width:500px;height:320px',
                                html: ''
                            },
                            {
                                type: 'hbox',
                                id: 'imgparamsid',
                                style: 'display:none;width:500px;',
                                height: '325px',
                                children: [
                                    {
                                        type: 'vbox',
                                        align: 'center',
                                        width: '400px',
                                        children: [
                                            {
                                                type: 'text',
                                                id: 'imgtitleid',
                                                label: lang.imgTitle,
                                                onChange: function () {
                                                    if (this.getValue()) {
                                                        updateTitle(this.getDialog(), this.getValue());
                                                    }
                                                }
                                            },
                                            {
                                                type: 'text',
                                                id: 'imgdescid',
                                                label: lang.imgDesc,
                                                onChange: function () {
                                                    if (this.getValue()) {
                                                        updateDescription(this.getDialog(), this.getValue());
                                                    }
                                                }
                                            },
                                            {
                                                type: 'html',
                                                id: 'imgpreviewid',
                                                style: 'width:400px;height:200px;',
                                                html: '<div>xx</div>'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                type: 'hbox',
                                align: 'center',
                                height: 110,
                                widths: [ '25%', '50%'],
                                children: [
                                    {
                                        type: 'vbox',
                                        children: [
                                            {
                                                type : 'select',
                                                id : 'pictWidthId',
                                                label : lang.pictWidth + ':',
                                                items : [
                                                    [ 900, 900 ],
                                                    [ 650, 650 ],
                                                    [ 450, 450 ]
                                                ],
                                                default : 900,
                                                style : 'width:100px;',
                                                commit : commitValue,
                                                setup : loadValue,
                                                onChange : commitValue
                                            },
                                            {
                                                type: 'checkbox',
                                                id: 'showTitleId',
                                                label: lang.showTitle,
                                                default: '',
                                                onChange: commitValue,
                                                commit: commitValue,
                                                setup: loadValue
                                            },
                                            {
                                                type: 'checkbox',
                                                id: 'openOnClickId',
                                                label: lang.openOnClick,
                                                default: 'checked',
                                                onChange: commitValue,
                                                commit: commitValue,
                                                setup: loadValue
                                            }
                                        ]
                                    },
                                    {
                                        type: 'select',
                                        id: 'imglistitemsid',
                                        label: lang.picturesList,
                                        multiple: false,
                                        style: 'height:120px;width:250px',
                                        items: [],
                                        onChange: function () {
                                            //unselectIfNotUnique(this);
                                            selectFirstIfNotUnique(this);
                                        }
                                    },
                                    {
                                        type: 'vbox',
                                        children: [
                                            {
                                                type: 'button',
                                                id: 'addImageBtn',
                                                style: 'margin-top:10px;' + getIconStyle('image'),
                                                label: '',
                                                title: lang.imgAdd,
                                                onClick: function () {
                                                    var dialog = this.getDialog();
                                                    uploadcare.openDialog().done(function (file) {
                                                        file.done(function (fileInfo) {
                                                            var url = fileInfo.cdnUrl;
                                                            if (editor.config.USE_PHP) {
                                                                editor.execCommand('uploadcareDialog', true);
                                                            } else {
                                                                if (fileInfo.isImage) {
                                                                    dialog.setValueOf('slideshowinfoid', 'txturlid', url);
                                                                }
                                                            }
                                                        });
                                                    });
                                                    $('.uploadcare-dialog').css('z-index', dialog.getElement().$.children[0].style.zIndex + 10);
                                                }
                                            },
                                            {
                                                type: 'button',
                                                id: 'previewBtn',
                                                style: getIconStyle('preview'),
                                                label: '',
                                                title: lang.previewMode,
                                                onClick: function () {
                                                    previewSlideShow(this.getDialog());
                                                }
                                            },
                                            {
                                                type: 'button',
                                                id: 'removeSelectedBtn',
                                                style: getIconStyle('removeformat'),
                                                label: '',
                                                title: lang.imgDelete,
                                                onClick: function () {
                                                    removeSelected(this.getDialog());
                                                }
                                            },
                                            {
                                                type: 'button',
                                                id: 'editSelectedBtn',
                                                style: getIconStyle('textfield'),
                                                label: '',
                                                title: lang.imgEdit,
                                                onClick: function () {
                                                    editSelected(this.getDialog());
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ],

        onLoad: function () {
        },

        // Invoked when the dialog is loaded.
        onShow: function () {
            this.slideshowDOM = null;
            this.openCloseStep = true;
            this.fakeImage = null;
            this.imagesList = [];
            this.params = [];

            // To get dimensions of poster image
            this.previewImage = editor.document.createElement('img');

            var slideshowDOM = null;

            var fakeImage = this.getSelectedElement();
            if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') == 'slideShow') {
                this.fakeImage = fakeImage;
                slideshowDOM = editor.restoreRealElement(fakeImage);
            }

            // Create a new <slideshow> slideshowDOM if it does not exist.
            if (!slideshowDOM) {
                this.params.push(['slideshowid', numbering('slideShow')]);

                // Insert placeHolder image
                initImgListFromFresh(this);
                // Invoke the commit methods of all dialog elements, so the dialog.params array get Updated.
                this.commitContent(this);
//				console.log( "Params New -> " + this.params );
//				console.log( "Images New -> " + this.imagesList );
            } else {
                this.slideshowDOM = slideshowDOM;
                // Get the reference of the slideshow Images Container
                var slideShowContainer = getImagesContainerBlock(slideshowDOM);
                if (slideShowContainer == null) {
                    alert("BIG Problem slideShowContainer !!");
                    return false;
                }
                var slideshowid = slideshowDOM.getAttribute('id');
                if (slideshowid == null) {
                    alert("BIG Problem slideshowid !!");
                    return false;
                }
                this.params.push(['slideshowid', slideshowid]);
                // a DOM has been found updated images List and Dialog box from this DOM
                initImgListFromDOM(this, slideShowContainer);

                // Init params Array from DOM
                // Copy all attributes to an array.
                var params = this.params;
                $.each(slideshowDOM.$.dataset, function (key, value) {
                    params.push([ key, value ]);
                });

                // Invoke the setup methods of all dialog elements, to set dialog elements values with DOM input data.
                this.setupContent(this, true);
                //updateFramePreview(this);
                this.newSlideShowMode = false;
            }
            this.openCloseStep = false;
            previewSlideShow(this);

            return true;
        },

        // This method is invoked once a user clicks the OK button, confirming the dialog.
        // I just will return false, as the real OK Button has been redefined
        //  -This was the only way I found to avoid dialog popup to close when hitting the keyboard "ENTER" Key !!
        onOk: function () {
//			var okr = this.okRefresh;
//			if (this.okRefresh == true) {
//				console.log('OKOKOK 0 :'+this.okRefresh);
//				this.okRefresh = false;
//				this.commitContent(this);
//				myVar = setTimeout(
//						function(obj){
//									obj.okRefresh = true;
//									},500, this);
//			}
            return false;
        },

        onHide: function () {
            cleanAll(this);
        }
    };
});