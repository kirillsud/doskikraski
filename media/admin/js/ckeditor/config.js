﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
UPLOADCARE_PUBLIC_KEY = "b8b53fbebb3fc4e20978";
UPLOADCARE_TABS = "file url vk facebook gdrive skydrive instagram";
UPLOADCARE_LOCALE = 'ru';

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = '/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = '/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl = '/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = '/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = '/kcfinder/upload.php?type=flash';

    config.extraPlugins = 'mediaembed,uploadcare,serverpreview,slideshow';
    config.serverPreview_Url = function() {
        return '/admin/userpage/preview?title=' + document.getElementsByName('page[title]')[0].value;
    }
};
