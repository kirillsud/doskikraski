<?php
define ('DOCROOT', __DIR__);
define ('APPROOT', DOCROOT . '/app');
define ('MODROOT', DOCROOT . '/modules');

// переопределяем настройки php
ini_set('display_errors', 1);

// инициализируем микро фреймворк silex
require_once DOCROOT . '/silex.phar';
$app = new Silex\Application();

// запускаем сайт
require_once DOCROOT . '/app/bootstrap.php';